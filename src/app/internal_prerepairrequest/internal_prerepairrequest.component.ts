import { Component, OnInit } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { IRepairRequest } from "../shared/Interfaces/IRepairRequest.interface";
import { RepairRequestService } from "../repair-requests/services/repair-request.service";

@Component({
    selector: "app-internal-prerepairrequest",
    templateUrl: "./internal_prerepairrequest.component.html",
    styleUrls: ["./internal_prerepairrequest.component.css"],
    providers: [RepairRequestService]
})
export class Internal_PreRepairRequestComponent implements OnInit {
    repairrequests: IRepairRequest[];
    type: boolean;

    constructor(private repairrequestservice: RepairRequestService) { }
    ngOnInit() {

        this.type = true;

        this.repairrequestservice.getByExternalandMaintenanceType(false, this.type).subscribe(
            repairrequestRes => {
                if (repairrequestRes["isSucceeded"] === true) {
                    this.repairrequests = repairrequestRes["data"];
                }

            });
    }

}