import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { JobCardsListReportComponent } from "./job-cards-list-report.component";

describe("JobCardsListReportComponent", () => {
    let component: JobCardsListReportComponent;
    let fixture: ComponentFixture<JobCardsListReportComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [JobCardsListReportComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(JobCardsListReportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
