export class JobCard {
    id: string;
    customerName: string;
    dateIn: Date;
    dateOut: Date;
    description: string;
    vehicleId: number;
    driverId: number;
    serviceTypeId: number;
    jobStatusId: number;
    comment: string;
    odometer: number;
}