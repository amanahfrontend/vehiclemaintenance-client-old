import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { WebService } from "app/shared/services/webService";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { JobCardStatus } from "../models/job-card-status.model";

@Injectable()
export class JobCardStatusService {

    constructor(private http: HttpClient, private config: WebService) { }

    getJobCardStatuses(): Observable<JobCardStatus[]> {
        const url = this.config.baseUrl + "JobCardStatus/Get";

        return this.http.get(url).map(data => {
            const result = <JobCardStatus[]>data;
            return result;
        });
    }

    addJobCardStatus(model): Observable<JobCardStatus> {
        const url = this.config.baseUrl + "JobCardStatus/Add";

        return this.http.post(url, model).map(
            data => {
                const result = <JobCardStatus>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    updateJobCardStatus(model): Observable<JobCardStatus> {
        const url = this.config.baseUrl + "JobCardStatus/Update";

        return this.http.put(url, model).map(
            data => {
                const result = <JobCardStatus>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    deleteJobCardStatus(id) {
        const url = this.config.baseUrl + "JobCardStatus/Delete/" + id;

        return this.http.delete(url).map(
            data => {
                const result = <JobCardStatus>data;

                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }
}