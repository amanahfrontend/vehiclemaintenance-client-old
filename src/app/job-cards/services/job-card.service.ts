import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { WebService } from "app/shared/services/webService";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { JobCard } from "../models/job-card.model";

@Injectable()
export class JobCardService {

    constructor(private http: HttpClient, private config: WebService) { }

    //getJobCards(type): Observable<IJobCard[]> {
    //    const url = this.config.baseUrl + "JobCard/GetByMaintenanceType/" + type;

    //    return this.http.get(url).map(data => {
    //        let result = <IJobCard[]>data;
    //        return result;
    //    });
    //}

    getOpenJobCards(): Observable<JobCard[]> {
        const url = this.config.baseUrl + "JobCard/GetByOpenStatus";

        return this.http.get(url).map(data => {
            const result = <JobCard[]>data;
            return result;
        });
    }

    getJobCards(): Observable<JobCard[]> {
        const url = this.config.baseUrl + "JobCard/GetJobCards";

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getbyCreatedDate(fleetNumber): Observable<JobCard[]> {
        const url =
            this.config.baseUrl +
            "JobCard/GetByCreatedDate?fleetNumber=" +
            fleetNumber;

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getnonApproved(): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetNonApprovedJobcards";

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    getbyMaintenanceAndserviceAndFleet(serviceType, maintenanceType, fleetNumber): Observable<JobCard[]> {

        const url = this.config.baseUrl +
            "JobCard/GetByMaintenanceAndserviceAndFleet?serviceType=" +
            serviceType +
            "&maintenanceType=" +
            maintenanceType +
            "&fleetNumber=" +
            fleetNumber;

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getbyMaintenanceAndserviceAndVehicleId(serviceType, maintenanceType, vehicleId): Observable<JobCard[]> {
        const url = this.config.baseUrl +
            "JobCard/GetByMaintenanceAndserviceAndvehicleId?serviceType=" +
            serviceType +
            "&maintenanceType=" +
            maintenanceType +
            "&vehicleId=" +
            vehicleId;

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getJobCardById(id): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetJobCard/" + id;

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    addJobCard(model): Observable<any> {
        model.createdByUserId = this.config.currentUserId;
        model.rowStatusId = 1;
        model.deliveredQty = 0;

        const url = this.config.baseUrl + "JobCard/Add";

        return this.http.post(url, model).map(
            data => {
                const result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            }
        );
    }

    addJobCardWithapprove(model): Observable<any> {
        const url = this.config.baseUrl + "JobCard/AddwithApprove";

        return this.http.post(url, model).map(
            data => {
                let result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            }
        );
    }

    updateJobCard(model): Observable<any> {
        model.ModifiedByUserId = this.config.currentUserId;
        model.rowStatusId = 1;

        const url = this.config.baseUrl + "JobCard/Update";

        return this.http.put(url, model).map(
            data => {
                let result = <any>data;

                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            }
        );
    }

    deleteJobCard(id) {
        let url = "{0}JobCard/Delete/{1}/{2}";

        url = url.replace("{0}", this.config.baseUrl).replace("{1}", id).replace("{2}", this.config.currentUserId);


        return this.http.delete(url).map(
            data => {
                let result = <JobCard>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            }
        );
    }

    GetByIds(ids): Observable<JobCard[]> {
        const url = this.config.baseUrl + "JobCard/GetByIds";

        return this.http.post(url, ids).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getJobCardsByVehicleId(vehicleId): Observable<JobCard[]> {
        const url = this.config.baseUrl + "JobCard/GetByVehicleId/" + vehicleId;

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getJobCardsOpenByVehicleId(vehicleId): Observable<JobCard[]> {
        const url = this.config.baseUrl + "JobCard/GetByOpenStatusOfVehicle/" + vehicleId;

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    preventiveReport(fleetNumber) {
        const url = this.config.baseUrl + "JobCard/GetServiceCountReport?fleetNumber=" + fleetNumber;

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getAllJobCardsWithSearch(vehicleId, query1, query2, query3): Observable<JobCard[]> {
        const urlStatic =
            "JobCard/SearchByID_CustomerType_Desc?vehicleId={0}&&ID={1}&&CustomerType={2}&&Desc={3}";

        const url = this.config.baseUrl +
            urlStatic.replace("{0}", vehicleId).replace("{1}", query1).replace("{2}", query2).replace("{3}", query3);

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    getAllJobCardsWithSearch2(vehicleId, query): Observable<JobCard[]> {
        const urlStatic = "JobCard/SearchByID_CustomerType_Desc?vehicleId={0}&&word={1}";
        const url = this.config.baseUrl + urlStatic.replace("{0}", vehicleId).replace("{1}", query);

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    public canAddJobCard(vehicleId: number): Observable<any> {
        const url = this.config.baseUrl + "JobCard/CanAddJobCard/" + vehicleId;

        return this.http.get(url).map(data => {
            const result = <any>data;
            return result;
        });
    }

    // [Mohammed.Osman] Seems [not-used]
    getAllExceptClosed(): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetAllExceptClosed";

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    // [Mohammed.Osman] Seems [not-used]
    getAllJobcardClosed(): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetAllClosed";

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    getNonCancelledNonClosedJobCards(vehicleId): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetNonCancelledNonClosedStatuses/" + vehicleId;

        return this.http.get(url, {}).map(data => {
            let result = <any>data;
            return result;
        });
    }

    getAllActiveJobCardsWithSearch2(vehicleId, query): Observable<JobCard[]> {
        const urlStatic = "JobCard/SearchActiveJobCards?vehicleId={0}&word={1}";
        const url = this.config.baseUrl + urlStatic.replace("{0}", vehicleId).replace("{1}", query);

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    // This method is used.
    getCancelledOrClosedJobCardsByVehicleId(vehicleId: number): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetCancelledOrClosedStatuses/" + vehicleId;

        return this.http.get(url, {}).map(data => {
            let result = <any>data;
            return result;
        });
    }

    getAllInactiveJobCardsWithSearch2(vehicleId, query): Observable<JobCard[]> {
        const urlStatic = "JobCard/SearchDeActiveJobCards?vehicleId={0}&word={1}";
        const url = this.config.baseUrl + urlStatic.replace("{0}", vehicleId).replace("{1}", query);

        return this.http.get(url).map(data => {
            let result = <JobCard[]>data;
            return result;
        });
    }

    // This method is used.
    searchJobCards(customerName, dateIn, dateOut, serviceTypeId, jobCardStatusId, vehiclePlateNo, vehicleFleetNo, jobCardId): Observable<any> {

        const queryStringParams =
            "customerName={0}&dateIn={1}&dateOut={2}&serviceTypeId={3}&jobCardStatusId={4}&vehiclePlateNo={5}&vehicleFleetNo={6}&jobCardId={7}";

        const url = this.config.baseUrl + "JobCard/SearchJobCards?" +
            queryStringParams.replace("{0}", customerName).replace("{1}", dateIn).replace("{2}", dateOut)
                .replace("{3}", serviceTypeId).replace("{4}", jobCardStatusId).replace("{5}", vehiclePlateNo)
                .replace("{6}", vehicleFleetNo).replace("{7}", jobCardId);

        return this.http.get(url).map(data => {
            const result = <any>data;
            return result;
        });
    }
}