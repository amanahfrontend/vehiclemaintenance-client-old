import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DialogService } from "../../shared/services/dialog.service";
import { TechnicianService } from "../services/technician.service";

@Component({
    selector: "app-technician-edit",
    templateUrl: "./technician-edit.component.html",
    providers: [TechnicianService]
})

export class TechnicianEditComponent implements OnInit {
    public visible = false;
    public visibleAnimate = false;
    model: any = {};

    @Output() dialogClosed = new EventEmitter();

    constructor(
        private technicianService: TechnicianService,
        private customDialogService: DialogService) {
    }

    ngOnInit() {

    }

    show(): void {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    public shows(model): void {

        Object.assign(this.model, model);

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    updateTechnician() {

        this.technicianService.updateTechnician(this.model).subscribe(
            technicianRes => {
                if (technicianRes["isSucceeded"] === true) {
                    this.visibleAnimate = false;
                    //this.customDialogService.alert("Done", "updated successfully", false);
                    setTimeout(() => this.visible = false, 300);

                    this.dialogClosed.emit();
                }
                else {
                    this.customDialogService.alert("Fail", "SomeThing wrong", false);
                }
            });
    }

    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
}
