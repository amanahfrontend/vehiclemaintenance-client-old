import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DialogService } from "../../shared/services/dialog.service";
import { TechnicianService } from "../services/technician.service";

@Component({
    selector: "app-technician-add",
    templateUrl: "./technician-add.component.html",
    providers: [TechnicianService]
})
export class TechnicianAddComponent implements OnInit {
    router: Router;
    public visible = false;
    public visibleAnimate = false;
    model: any = {};
    technicians: any = [];

    constructor(
        router: Router,
        private technicianService: TechnicianService,
        private customDialogService: DialogService,
        public dialogRef: MatDialogRef<TechnicianAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {

        this.router = router;
    }

    ngOnInit() { }

    show(): void {
        this.model = {};
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    hide(): void {
        this.model = {};
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    saveTechnician() {

        this.technicianService.addTechnician(this.model).subscribe(
            technicianRes => {
                if (technicianRes["isSucceeded"] === true) {

                    this.technicianService.getTechnicians().subscribe(
                        () => {
                            this.technicians = technicianRes["data"];
                            return this.technicians;
                        });

                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);
                    this.customDialogService.alert("Done", "added successfully", false);
                    this.dialogRef.close();
                }
                else {
                    this.customDialogService.alert("Fail", "some thing wrong", false);
                    this.dialogRef.close();
                }
            });
    }

    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);

        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
}
