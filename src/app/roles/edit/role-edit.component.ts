import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DialogService } from "../../shared/services/dialog.service";
import { RoleService } from "../services/role.service";

@Component({
    selector: "app-role-edit",
    templateUrl: "./role-edit.component.html",
    styleUrls: ["./role-edit.component.css"],
    providers: [RoleService]
})
export class RoleEditComponent implements OnInit {
    public visible = false;
    public visibleAnimate = false;
    model: any = {};

    @Output() dialogClosed = new EventEmitter();

    constructor(
        private roleService: RoleService,
        private customDialogService: DialogService) {
    }

    ngOnInit() {

    }

    show(): void {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    showDialgo(model): void {

        Object.assign(this.model, model);

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    updateRole() {

        this.roleService.updateRole(this.model).subscribe(
            roleRes => {
                if (roleRes["isSucceeded"] === true) {
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);

                    this.dialogClosed.emit();
                }
                else {
                    this.customDialogService.alert("Fail", "Something wrong", false);
                }
            });
    }
}
