import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { IRole } from "../../shared/Interfaces/IRole.interface";
import { WebService } from "../../shared/services/webService";

@Injectable()
export class RoleService {

    constructor(private http: HttpClient, private config: WebService) { }

    getRoles(): Observable<IRole[]> {
        const url = this.config.baseUrl + "Role/Get";

        return this.http.get(url).map(data => {
            const result = <IRole[]>data;
            return result;
        });
    }

    addRole(model): Observable<IRole> {
        const url = this.config.baseUrl + "Role/Add";

        return this.http.post(url, model).map(
            data => {
                const result = <IRole>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            });
    }

    updateRole(model): Observable<IRole> {
        const url = this.config.baseUrl + "Role/Update";

        return this.http.put(url, model).map(
            data => {
                const result = <IRole>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            });
    }

    deleteRole(roleName) {
        const url = this.config.baseUrl + "Role/Delete/" + roleName;

        return this.http.delete(url).map(
            data => {
                const result = <IRole>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            });
    }
}
