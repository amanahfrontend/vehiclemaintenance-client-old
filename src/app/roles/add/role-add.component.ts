import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DialogService } from "../../shared/services/dialog.service";
import { RoleService } from "../services/role.service";

@Component({
    selector: "app-Role-add",
    templateUrl: "./role-add.component.html",
    providers: [RoleService]
})
export class RoleAddComponent implements OnInit {
    router: Router;
    public visible = false;
    public visibleAnimate = false;
    model: any = {};

    constructor(
        private roleService: RoleService,
        private customDialogService: DialogService,
        public dialogRef: MatDialogRef<RoleAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {}

    show(): void {
        this.model = {};
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    hide(): void {
        this.model = {};
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    saveRole() {

        this.roleService.addRole(this.model).subscribe(roleRes => {
            if (roleRes["isSucceeded"] === true) {
                this.visibleAnimate = false;
                setTimeout(() => this.visible = false, 300);
                this.customDialogService.alert("Done", "added successfully", false);
                this.dialogRef.close();
            } else {
                this.dialogRef.close();
                this.customDialogService.alert("Fail", "some thing wrong", false);
            }
        });
    }
}
