import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { WebService } from "./webService";

@Injectable()
export class GarageReceiptReportService {

    constructor(private http: Http, private config: WebService) {
        
    }

    // Mohammed Osman.
    getDailyReceiptReport(dailyServiceSearchParams): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetDailyReceiptReport";
        return this.http.post(url, dailyServiceSearchParams).map(response => response.json());
    }

    //getAllGarageData(date): Observable<any> {
    //    const staticUrl = "JobCard/GetDailyWorkOfGarage/{0}";
    //    const url = this.config.baseUrl + staticUrl.replace("{0}", date);
    //    return this.http.get(url).map(response => response.json());
    //}

    //getAllDailyReceipt(): Observable<any> {
    //    const url = this.config.baseUrl + "JobCard/GetDailyReceipt";
    //    return this.http.post(url, {}).map(response => response.json());
    //}

    //getAllDailyBroughtReceipt(body): Observable<any> {
    //    const url = this.config.baseUrl + "JobCard/GetDailyBroughtReceipt";
    //    return this.http.post(url, body).map(response => response.json());
    //}

    //getAllDailyReceiptExceptBrought(body): Observable<any> {
    //    const url = this.config.baseUrl + "JobCard/GetDailyReceiptExceptBrought";
    //    return this.http.post(url, body).map(response => response.json());
    //}
}
