import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { WebService } from "./webService";

@Injectable()
export class dashboardService {

    constructor(private http: Http, private config: WebService) { }

    getOpenJobCardsCount(): Observable<any> {
        const url = this.config.baseUrl + "JobCard/OpenJobCardsCount";

        return this.http.get(url).map(data => {
            const result = <any>data.json();
            return result;
        });
    }

    getOpenRepairRequestCount(): Observable<any> {
        const url = this.config.baseUrl + "RepairRequest/OpenRepairRequestsCount";

        return this.http.get(url).map(data => {
            const result = <any>data.json();
            return result;
        });
    }

    getUsersCount(): Observable<any> {
        const url = this.config.baseUrl + "user/Count";

        return this.http.get(url).map(data => {
            const result = <any>data.json();
            return result;
        });
    }

    getRepairRequestChart(): Observable<any> {
        const url = this.config.baseUrl + "RepairRequest/GetChart";

        return this.http.get(url).map(data => {
            const result = <any>data.json();
            return result;
        });
    }

    getJobCardChart(): Observable<any> {
        const url = this.config.baseUrl + "JobCard/GetChart";

        return this.http.get(url).map(data => {
            let result = <any>data.json();
            return result;
        });
    }
}