import { Component, OnInit, Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { IDriver } from '../Interfaces/IDriver.Interface';
import { HttpModule, Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { WebService } from './webService'

@Injectable()
export class equipmentTransferService {

    constructor(private _http: Http, private config: WebService) {}

    getAllEquipments(): Observable<any[]> {
        const url = this.config.baseUrl + "UsedMaterials/GetAllEquipmentTransfer";
        return this._http.get(url)
            .map(response => response.json());
    }

    getAllEquipmentsByDate(date): Observable<any[]> {
        const staticUrl = "UsedMaterials/GetEquipmentTransferByDate?date={0}";
        const url = this.config.baseUrl + staticUrl.replace('{0}', date);
        return this._http.get(url)
            .map(response => response.json())
    }

    getAllEquipmentsByFleetFrom(fleetFrom): Observable<any[]> {
        const staticUrl = "UsedMaterials/GetEquipmentTransferByVehicleFrom?fleetNumber={0}";
        const url = this.config.baseUrl + staticUrl.replace('{0}', fleetFrom);
        return this._http.get(url)
            .map(response => response.json());
    }

    getAllEquipmentsByFleetTo(fleetFrom): Observable<any[]> {
        const staticUrl = "UsedMaterials/GetEquipmentTransferByVehicleTo?fleetNumber={0}";
        const url = this.config.baseUrl + staticUrl.replace('{0}', fleetFrom);
        return this._http.get(url)
            .map(response => response.json());
    }

}