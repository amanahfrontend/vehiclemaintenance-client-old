﻿import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment.prod";

@Injectable()
export class CommonService {
    // Reports Web API Service URL should ends with "/report.aspx".
    reportsBaseUrl: string = "http://localhost:18521/report.aspx";

    // Security Properties............................. {Start}

    get currentUserId() {
        const userId = JSON.parse(localStorage.getItem("userId"));
        return userId;
    }

    get currentUserRole() {
        const userRole = sessionStorage.getItem("userRole");
        return userRole;
    }

    get currentUserIsManager() {
        return this.currentUserRole === "Manager";
    }

    get currentUserIsAdmin() {
        return this.currentUserRole === "Admin";
    }

    get currentUserIsEmployee() {
        return this.currentUserRole === "Employee";
    }

    // Security Properties............................. {End}

    print(): void {
        const contentToBePrinted = document.getElementById("print-section").innerHTML;
        const popupWindow = window.open("", "_blank", "top=0,left=0,height=100%,width=auto");

        popupWindow.document.open();
        popupWindow.document.write(`
              <html>
                <head>
                  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap1.min.css"/>
                  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
                  <link rel="stylesheet" type="text/css" href="assets/css/printingDocumentStyles.css"/>
                </head>
                <body onload="window.print();window.close()">${contentToBePrinted}</body>
              </html>`);

        popupWindow.document.close();
    }

    // queryStringParams: "customerNameId=1&startDate=2018-12-25&name=abc";
    print2(reportId: number, queryStringParams: string): void {
        const url = this.reportsBaseUrl + "?id=" + reportId + "&" + queryStringParams;
        window.open(url, "_blank", "top=40,left=150,height=750px,width=950px");
    }

    log(text: string): void {
        if (!environment.production) {
            console.log(text);
        }
    }

    // Date Time Methods ..................................... {Start}

    getDateTimeAsString(dateObj: any): string {

        if (this.isDateObjIsString(dateObj)) {
            // The dateObj is of type string. return it as it is.
            return dateObj;
        }

        const fullYear = dateObj.getFullYear();
        const month = dateObj.getMonth();
        const day = dateObj.getDate();
        const hours = dateObj.getHours();
        const minutes = dateObj.getMinutes();
        const seconds = dateObj.getSeconds();

        const utcDate = new Date(Date.UTC(fullYear, month, day, hours, minutes, seconds));

        return utcDate.toISOString();// "2018-12-01T00:00:00.000Z"
    }

    getDateAsString(dateObj: any): string {
        // return string like "2018-12-01".
        return this.getDateTimeAsString(dateObj).split("T")[0];
    }

    isDateObjIsString(dateObj: any): boolean {
        // Date has not length.
        // String has a length.
        return (dateObj.length !== undefined);
    }

    convertStringToDate(dateString: string): Date {
        const newDate = new Date(dateString);
        return newDate;
    }

    // Date Time Methods ..................................... {End}

    keyPressArabic(event: any) {
        const pattern = /^[\u0600-\u06ff ]+$/g;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }

    preventKeyPressArabic(event: any) {
        const pattern = /^[A-Za-z -]+$/g;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }
}