import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/observable';
import { ISparePartStatus } from '../Interfaces/ISparePartStatus.interface';
import { WebService } from './webService';

@Injectable()
export class SparePartStatusService {

    constructor(private http: Http, private config: WebService) { }

    getSparePartStatus(): Observable<ISparePartStatus[]> {
        const url = this.config.baseUrl + "SparePartStatus/Get";

        return this.http.get(url).map(data => {
            let result = <ISparePartStatus[]>data.json();
            return result;
        });
    }

    addSparePartStatus(model): Observable<ISparePartStatus> {
        model.fK_Status_ID = 1;
        const url = this.config.baseUrl + "SparePartStatus/Add";

        return this.http.post(url, model).map(
            data => {
                let result = <ISparePartStatus>data.json();
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    updateSparePartStatus(model): Observable<ISparePartStatus> {
        model.fK_Status_ID = 2;
        const url = this.config.baseUrl + "SparePartStatus/Update";

        return this.http.put(url, model).map(
            data => {
                let result = <ISparePartStatus>data.json();
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    deleteSparePartStatus(id) {
        const url = this.config.baseUrl + "SparePartStatus/Delete/" + id;

        return this.http.delete(url).map(
            data => {
                let result = <ISparePartStatus>data.json();

                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

}
