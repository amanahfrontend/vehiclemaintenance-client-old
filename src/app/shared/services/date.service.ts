import { Injectable } from "@angular/core";

@Injectable()
export class DateService {

    getDateTimeAsString(dateObj: any): string {

        if (this.isDateObjIsString(dateObj)) {
            // The dateObj is of type string. return it as it is.
            return dateObj;
        }

        const fullYear = dateObj.getFullYear();
        const month = dateObj.getMonth();
        const day = dateObj.getDate();
        const hours = dateObj.getHours();
        const minutes = dateObj.getMinutes();
        const seconds = dateObj.getSeconds();

        const utcDate = new Date(Date.UTC(fullYear, month, day, hours, minutes, seconds));

        return utcDate.toISOString();// "2018-12-01T00:00:00.000Z"
    }

    getDateAsString(dateObj: any): string {
        // return string like "2018-12-01".
        return this.getDateTimeAsString(dateObj).split("T")[0];
    }

    isDateObjIsString(dateObj: any): boolean {
        // Date has not length.
        // String has a length.
        return (dateObj.length !== undefined);
    }

    convertStringToDate(dateString: string): Date {
        const newDate = new Date(dateString);
        return newDate;
    }

    // 1: Date1 > Date2
    // 0: Date1 = Date2
    // 2: Date1 < Date2
    compareTwoDates(date1: Date, date2: Date): number {
        date1.setSeconds(0);
        date2.setSeconds(0);

        if (date1.getTime() > date2.getTime()) {
            return 1; // 1: Date1 > Date2
        }

        if (date1.getTime() === date2.getTime()) {
            return 0; // 0: Date1 = Date2
        }

        return 2; // 2: Date1 < Date2
    }
}
