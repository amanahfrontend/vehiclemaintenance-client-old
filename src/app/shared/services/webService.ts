import { Injectable } from "@angular/core";

@Injectable()
export class WebService {
    // Web API Service URL should ends with "/api/".
    baseUrl = "http://localhost:2730/api/";

    get currentUserId() {
        var userId = JSON.parse(localStorage.getItem("userId"));
        return userId;
    }
}
