import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { IFile } from "../Interfaces/IFile.interface";
import { WebService } from "./webService";

@Injectable()
export class fileService {

    constructor(private _http: Http, private config: WebService) {}

    addFile(model): Observable<IFile[]> {
        const url = this.config.baseUrl + "File/SubmitFile";
        return this._http.post(url, model).map(
            data => {
                let temp = <IFile[]>data.json();
                return temp;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    deletefile(id): Observable<IFile> {
        const url = this.config.baseUrl + "File/Delete/" + id;
        return this._http.delete(url).map(
            data => {
                let temp = <IFile>data.json();
                return temp;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    UpdateChoosedVendor(repairId, FK_Vendor_ID): Observable<IFile[]> {
        const url = this.config.baseUrl +
            "File/UpdateChoosedVendor?repairId=" +
            repairId +
            "&FK_Vendor_ID=" +
            FK_Vendor_ID;
        console.log(url);
        return this._http.request(url).map(data => {
            let temp = <IFile[]>data.json();
            return temp;
        });
    }
}
