import { Component, OnInit, Injectable } from '@angular/core';
import {Observable} from 'rxjs/observable';
import {IJobCard_Technician} from '../Interfaces/IJobCard_Technician.interface';
import {HttpModule, Http,Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { WebService } from './webService'

@Injectable()

  export class JobCard_TechnicianService {

      constructor(private _http:Http, private config: WebService) {}

      getJobCard_Technicians():Observable<IJobCard_Technician[]> {
          const url = this.config.baseUrl + "JobCard_Technician/Get";
        return this._http.get(url).map(data=>{
        let temp = <IJobCard_Technician[]>data.json();
        return temp;
        });	
      }
      addJobCard_Technician(model):Observable<IJobCard_Technician>{
        const url= this.config.baseUrl + "JobCard_Technician/Add"
        return this._http.post(url,model).map(
        data => {
        let temp = <IJobCard_Technician>data.json();
        return temp;
                 },
        error => {
         console.log(JSON.stringify(error.json()));
        }) ;
      }
      updateJobCard_Technician(model):Observable<IJobCard_Technician>{
        const url= this.config.baseUrl + "JobCard_Technician/Update"
        return this._http.put(url,model).map(
        data => {
        let temp = <IJobCard_Technician>data.json();
        return temp;
                 },
        error => {
         console.log(JSON.stringify(error.json()));
        }) ; 	      
      }
      deleteJobCard_Technician(id){
        const url= this.config.baseUrl + "JobCard_Technician/Delete/"+id;
        return this._http.delete(url).map(
        data => {
        let temp = <IJobCard_Technician>data.json();
        console.log(temp);
        return temp;
                 },
        error => {
         console.log(JSON.stringify(error.json()));
         }) ; 	      
      }
      getByjobcardId(id) :Observable<IJobCard_Technician>{
        const url= this.config.baseUrl + "JobCard_Technician/GetByJobCardID/"+id;  
        return this._http.get(url).map(data=>{
        let temp = <IJobCard_Technician>data.json();
        return temp;
        });	
      }

      GetJobcardsByTechniciansAndDate(TechnicianId,startDate,endDate) :Observable<IJobCard_Technician>{
        const url= this.config.baseUrl + "JobCard_Technician/GetJobcardsByTechniciansAndDate?TechnicianId="+TechnicianId+"&startDate="+startDate+"&endDate="+endDate;  
        return this._http.get(url).map(data=>{
        let temp = <IJobCard_Technician>data.json();
        return temp;
        });	
      } 
  }