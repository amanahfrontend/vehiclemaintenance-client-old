import { Injectable } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { IUsedMaterials } from "../Interfaces/IUsedMaterials.interface";
import { WebService } from "./webService";
import { HttpClient } from "@angular/common/http";

//[Osman: Rename this class to RepairRequestMaterialService]
@Injectable()
export class UsedMaterialsService {

    constructor(private http: HttpClient, private config: WebService) { }

    getUsedMaterials(): Observable<IUsedMaterials[]> {
        const url = this.config.baseUrl + "RepairRequestMaterial/Get";

        return this.http.get(url).map(data => {
            const result = <IUsedMaterials[]>data;
            return result;
        });
    }

    addRepairRequestMaterial(model): Observable<IUsedMaterials> {
        model.createdByUserId = this.config.currentUserId;
        model.rowStatusId = 1;

        const url = this.config.baseUrl + "RepairRequestMaterial/Add";

        return this.http.post(url, model).map(
            data => {
                const result = <IUsedMaterials>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    updateRepairRequestMaterial(model): Observable<IUsedMaterials> {
        model.modifiedByUserId = this.config.currentUserId;
        model.rowStatusId = 1;

        const url = this.config.baseUrl + "RepairRequestMaterial/Update";

        return this.http.put(url, model).map(
            data => {
                const result = <IUsedMaterials>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    deleteRepairRequestMaterial(repairRequestMaterialId: number) {
        let url = "{0}RepairRequestMaterial/Delete/{1}/{2}";

        url = url.replace("{0}", this.config.baseUrl).replace("{1}",
            repairRequestMaterialId.toString()).replace("{2}", this.config.currentUserId);

        return this.http.delete(url).map(
            data => {
                const result = <IUsedMaterials>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    getByRepairRequestId(repairRequestId): Observable<IUsedMaterials> {
        const url = this.config.baseUrl + "RepairRequestMaterial/GetByRepairRequestID/" + repairRequestId;

        return this.http.get(url).map(data => {
            const result = <IUsedMaterials>data;
            return result;
        });
    }

    getByjobcardId(id): Observable<IUsedMaterials> {
        const url = this.config.baseUrl + "RepairRequestMaterial/GetByJobCardID/" + id;

        return this.http.get(url).map(data => {
            const result = <IUsedMaterials>data;
            return result;
        });
    }

    getByRepairRequestIDs(repairrequestIds): Observable<IUsedMaterials[]> {
        const url = this.config.baseUrl + "RepairRequestMaterial/GetByRepairRequestIDs";

        return this.http.post(url, repairrequestIds).map(data => {
            const result = <IUsedMaterials[]>data;
            return result;
        });
    }

    getRepaireRequestWithSearch(query): Observable<IUsedMaterials[]> {
        const urlStatic = "RepairRequestMaterial/SearchBysparePartSerialNumber_cost?word={0}";
        const url = this.config.baseUrl + urlStatic.replace("{0}", query);

        return this.http.get(url).map(data => {
            const result = <IUsedMaterials[]>data;
            return result;
        });
    }
}
