export interface IUploadFile {
    fileContent: string;
    fileName: string;
    fileRelativePath: string;
}