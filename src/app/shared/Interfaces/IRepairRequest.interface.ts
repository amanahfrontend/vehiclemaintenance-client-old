import { IFile } from "./IFile.interface";

export interface IRepairRequest{
FK_User_ID:string;
FK_RepairRequestType_ID:number;
FK_JobCard_ID:number;
FK_File_URLs:string[];
FK_File_IDs:number[];
FK_RepairRequestStatus_ID:number;
RepairRequestType_Name:string;
RepairRequestStatus_Name:string;
Maintenance_Type:boolean;
User_Name:string;
Files:IFile[];
DriverEmployeeId:string;
FK_ServiceType_ID:number;
ServiceType_Name:string;
Duration:number;
cost:number;
External:boolean;
id:string;
}