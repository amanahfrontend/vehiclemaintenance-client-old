export interface IUsedMaterials {
    repairRequestId: number;

    technicianId: number;
    technicianEmployeeId: string;
    amount: number;
    sparePartId: number;
    sparePartSerialNumber: string;
    sparePartStatusId: number;
    sparePartStatusName: string;
    fleetNumber: string;
    cost: number;
    jobCardId: number;
    tranferFromVehicleId: number;
    capacity: string;
    message: string;
}