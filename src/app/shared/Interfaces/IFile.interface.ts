import { IUploadFile } from "./IUploadFile.interface";

export interface IFile{
    id:number;
    FileName:string;
    FK_Vendor_ID:number;
    Vendor_Name:string;
    FileUrl:string;
    Selected:boolean;
    File:IUploadFile;
    FK_RepairRequest_ID:number;
  }