export interface IType {
    id: number;
    fileName: string;
    fileUrl: string;
}