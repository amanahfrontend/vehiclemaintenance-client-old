export interface ISparePartsRequest{ 
    id:number;
    FK_RepairRequest_ID:number;
    FK_Technician_ID:number;
    TechnicianEmployeeId:string; 
    StartDate:Date;
    EndDate:Date;
    Amount:number;
    FK_Status_ID:number;
    FK_Inventory_ID:number;
    SparePart_Name:string;
    Description:string;
    Cost:number;
    SerialNumber:string;
    comments:string;
    source:string;
}