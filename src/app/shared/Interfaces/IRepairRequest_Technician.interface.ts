export interface IRepairRequest_Technician {
    repairRequestId: number;
    technicianId: number;
    technicianEmployeeId: string;
    startTime: Date;
    endTime: Date
    amount: string;
    removed: boolean;
    deletedDate: Date;
    message: string;
}