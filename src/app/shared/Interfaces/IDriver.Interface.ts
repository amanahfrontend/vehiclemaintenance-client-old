export interface IDriver {
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    phoneNumber: string;
    title: string;
    birthDate: string;
    ssn: string;
    licenseIssuedDate: Date;
    licenseIssuedState: string;
    licenseNumber: string;
    active: boolean;
    employeeId: string;
}