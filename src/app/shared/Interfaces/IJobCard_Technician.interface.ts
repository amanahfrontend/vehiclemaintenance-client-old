export interface IJobCard_Technician{
    FK_Technician_ID:number;
    fK_JobCard_ID :number;
    TechnicianEmployeeId:string;
    StartTime:Date;
    EndTime :Date
    Amount:string;
}
