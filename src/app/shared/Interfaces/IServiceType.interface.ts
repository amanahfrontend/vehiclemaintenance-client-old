export interface IServiceType {

    id: number;
    nameAr: string;
    nameEn: string;
    period: number;
}
