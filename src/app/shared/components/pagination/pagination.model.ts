﻿export class Pagination {
    pageNo: number;
    pageSize: number;

    totalRecords: number;// Count of total records.
}