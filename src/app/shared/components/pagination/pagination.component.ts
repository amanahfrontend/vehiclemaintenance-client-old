import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Pagination } from "./pagination.model";

@Component({
    selector: "app-pagination",
    templateUrl: "./pagination.component.html",
    styleUrls: ["./pagination.component.css"]
})
export class PaginationComponent implements OnInit {

    private pTotalRecords: number;

    @Input()
    set totalRecords(value: number) {
        this.pTotalRecords = value;
        this.calculateMaxPages();

        // Add total records as PageSize
        if (value > this.pageSizesList[this.pageSizesList.length - 1]) {
            this.pageSizesList.push(value);
        }
    }

    get totalRecords(): number {
        return this.pTotalRecords;
    }

    @Input() pageSize: number;
    @Output() pageChanged = new EventEmitter();

    maxPages: number;// Total number of pages. Ex. 20: means twenty pages from 1 to 20.
    maxDisplayedPages: number = 10; // Max pages numbers displayed by the pager. Ex: P1,P2,...,P5
    currentPageNo: number;
    pageSizesList: number[] = [30, 50, 100, 200, 500];
    pagesLinks: any[] = [];
    pagerStartPage: number = 1;
    pagination: Pagination = { pageNo: 1, pageSize: 30, totalRecords: 0 };

    ngOnInit() {
        this.setPaginationModel(1, this.pageSizesList[0] ? this.pageSizesList[0] : 50);

        this.calculateMaxPages();
    }

    setFirstPage() {
        if (this.currentPageNo === 1) {
            return;
        }

        this.pagerStartPage = 1;
        this.setPagesLinks(this.pagerStartPage);

        this.setPageNo(1);
    }

    setPreviousPage() {
        if (this.currentPageNo === 1) {
            return;
        }

        if (this.currentPageNo === this.pagesLinks[0]) {
            // Go to previous set of pages and select last page.
            this.setPagerStartPage(-1);
        } else {
            // Go to previous page within the displayed set of pages.
            this.setPageNo(this.currentPageNo - 1);
        }
    }

    setNextPage() {

        if (this.currentPageNo === this.maxPages) {
            return;
        }

        if (this.currentPageNo === this.pagesLinks[this.pagesLinks.length - 1]) {
            this.setPagerStartPage(1); // Go to next set of pages and select first page.
        } else {
            // Go to next page within the displayed set of pages.
            this.setPageNo(this.currentPageNo + 1);
        }
    }

    setLastPage() {
        if (this.currentPageNo === this.maxPages) {
            return;
        }

        this.pagerStartPage = this.maxPages - this.maxDisplayedPages + 1;
        this.setPagesLinks(this.pagerStartPage);

        this.setPageNo(this.maxPages);
    }

    setPageNo(pageNo: number) {
        this.setPaginationModel(pageNo, this.pagination.pageSize);
        this.pageChanged.emit(this.pagination);// Fire event
    }

    setPageSize(pageSize: number) {

        this.setPaginationModel(1, pageSize);
        this.calculateMaxPages();

        this.pageChanged.emit(this.pagination);
    }

    // flag is 1 or -1; 1: for next pages set; -1: for previous pages set.
    setPagerStartPage(flag: number) {

        if (flag === -1) {

            // Previous Pages Set
            if (this.pagerStartPage <= this.maxDisplayedPages) {
                this.pagerStartPage = this.maxDisplayedPages + 1;
            }
        } else {

            // Next Pages Set
            if (this.pagerStartPage + this.maxDisplayedPages >= this.maxPages) {
                this.pagerStartPage = this.maxPages - (2 * this.maxDisplayedPages) + 1;
            }
        }

        this.pagerStartPage += (flag * this.maxDisplayedPages);
        this.setPagesLinks(this.pagerStartPage);

        this.setPaginationModel(this.pagerStartPage, this.pageSize);

        if (flag === -1) {
            // In case of "Back set of pages", select last page.
            this.currentPageNo = this.pagerStartPage + this.maxDisplayedPages - 1;
        }

        this.pageChanged.emit(this.pagination);
    }

    calculateMaxPages() {

        if (!this.totalRecords) {
            return;
        }

        // Calculate max pages.
        this.maxPages = Math.floor(this.totalRecords / this.pagination.pageSize);

        if (this.totalRecords % this.pagination.pageSize > 0) {
            this.maxPages++;
        }

        this.pagerStartPage = 1;
        this.setPagesLinks(this.pagerStartPage);
    }

    setPagesLinks(startPage: number): void {

        var endPage = startPage + this.maxDisplayedPages - 1;

        if (endPage > this.maxPages) {
            endPage = this.maxPages;
        }

        this.pagesLinks = [];

        for (let i = startPage; i <= endPage; i++) {
            this.pagesLinks.push(i);
        }
    }

    setPaginationModel(pageNo: number, pageSize: number) {
        this.currentPageNo = pageNo;

        this.pagination.pageNo = pageNo;
        this.pagination.pageSize = pageSize;
    }
}
