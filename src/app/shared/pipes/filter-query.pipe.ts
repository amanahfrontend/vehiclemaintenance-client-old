import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "FilterQuery" })
export class FilterQueryPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            
            input = input.valueOf();

            return value.filter((el: any) => {
                const fleetNumber = el.fleetNumber;

                return (fleetNumber != undefined && fleetNumber.valueOf().indexOf(input) > -1,
                    el.jobCrd != undefined && el.jobCard.valueOf().indexOf(input) > -1,
                    el.repairId != undefined && el.repairId.valueOf().indexOf(input) > -1,
                    el.spareId != undefined && el.spareId.valueOf().indexOf(input) > -1);
            });
        }

        return value;
    }
}