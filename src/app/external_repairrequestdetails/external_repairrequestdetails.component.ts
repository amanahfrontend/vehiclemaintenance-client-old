import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { RepairRequestSparePartService } from "../repair-request-spare-parts/services/repair-request-spare-part.service";
import { RepairRequestService } from "../repair-requests/services/repair-request.service";
import { IFile } from "../shared/Interfaces/IFile.interface";
import { IRepairRequest } from "../shared/Interfaces/IRepairRequest.interface";
import { ISparePartsRequest } from "../shared/Interfaces/ISparePartsRequest.interface";

@Component({
    selector: "app-external_repairrequestdetails",
    templateUrl: "./external_repairrequestdetails.component.html",
    styleUrls: ["./external_repairrequestdetails.component.css"],
    providers: [RepairRequestService, RepairRequestSparePartService]
})
export class External_RepairRequestDetailsComponent implements OnInit {
    repairrequest: IRepairRequest[];
    id: number;
    model: any = {};
    files: IFile[] = [];
    sparepartsRequest: ISparePartsRequest[];
    private sub: any;
    isActive: boolean = false;
    fk_repairrequest: number;
    userRole: string;

    constructor(private router: Router,
        private dialogService: MdlDialogService,
        private repairRequestService: RepairRequestService,
        private sparepartsRequestService: RepairRequestSparePartService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.isActive = true;
        this.userRole = sessionStorage.getItem("userRole");

        this.sub = this.route.params.subscribe(params => {
            this.id = +params["id"];
            this.fk_repairrequest = this.id;
        });

        this.repairRequestService.getRepairRequestById(this.id).subscribe(
            repairRequestRes => {
                if (repairRequestRes["isSucceeded"] === true) {
                    this.repairrequest = repairRequestRes["data"];
                    this.model = repairRequestRes["data"];

                    this.sparepartsRequestService.getByRepairRequest(this.fk_repairrequest, 1).subscribe(
                        sparepartsRes => {
                            if (sparepartsRes["isSucceeded"] === true) {
                                this.sparepartsRequest = sparepartsRes["data"];
                                this.isActive = false;

                                console.log(JSON.stringify(this.sparepartsRequest));
                            }
                            return this.sparepartsRequest;
                        });

                    this.files = this.model.files;
                    console.log(this.files);
                    this.isActive = false;
                }

                return this.repairrequest;
            });
    }

    delete() {
        const question = "Are you sure, you want to delete this?";
        let result = this.dialogService.confirm(question, "No", "Yes");

        // if you need both answers
        result.subscribe(() => {

            this.repairRequestService.deleteRepairRequest(this.id).subscribe(
                repairrequestRes => {
                    
                    if (repairrequestRes["isSucceeded"] === true) {
                        this.router.navigateByUrl("/home/schrepairrequests");
                    }
                });
        });
    }

    isManager() {
        if (this.userRole === "Manager") {
            return true;
        } else {
            return false;
        }
    }
}
