import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DialogService } from "../../shared/services/dialog.service";
import { SparePartService } from "../services/spare-part.service";
import { SparePart } from "../models/spare-part.model";
import { SparePartAddEditComponent } from "../add-edit/spare-part-add-edit.component";

@Component({
    selector: "app-spare-parts-list",
    templateUrl: "./spare-parts-list.component.html",
    providers: [SparePartService, DialogService]
})
export class SparePartsListComponent implements OnInit {
    spareParts: SparePart[];
    isActive: boolean = false;

    searchParams: {
        name: "";
        serialNo: "";
        category: "";
    };

    constructor(
        private sparePartService: SparePartService,
        private customDialogService: DialogService,
        public dialog: MatDialog
    ) { }

    ngOnInit() {

        this.searchParams = {
            name: "",
            serialNo: "",
            category: ""
        };

        this.isActive = true;
        this.getSpareParts();
    }

    openAddEditSparePartDialog(id: number): void {

        const dlgOptions = {
            width: "650px",
            height: "565px",
            data: { id: id }
        };

        let dialogRef = this.dialog.open(SparePartAddEditComponent, dlgOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.getSpareParts();
        });
    }

    getSpareParts() {
        this.sparePartService.getSpareParts().subscribe(
            sparePartsRes => {

                if (sparePartsRes["isSucceeded"] === true) {
                    this.spareParts = sparePartsRes["data"];
                    this.isActive = false;
                }
            });
    }

    deleteSparePart(id) {

        const question = "Do you really want to delete this record?";
        const result = this.customDialogService.confirm("Confirm", question, true);

        // if you need both answers
        result.subscribe(
            () => {

                this.sparePartService.deleteSparePart(id).subscribe(
                    sparePartRes => {

                        if (sparePartRes["isSucceeded"] === true) {
                            this.getSpareParts();
                        }
                    });
            },
            () => {
                console.log("declined");
            });
    }

    searchSpareParts() {
        this.isActive = true;

        this.sparePartService.searchSpareParts(this.searchParams).subscribe(
            sparePartRes => {
                if (sparePartRes["isSucceeded"] === true) {
                    this.spareParts = sparePartRes["data"];
                    this.isActive = false;
                }

            });
    }
}
