import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Response } from "@angular/http";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { WebService } from "../../shared/services/webService";
import { SparePart } from "../models/spare-part.model";

@Injectable()
export class SparePartService {
    constructor(private http: HttpClient, private config: WebService) { }

    search(term: string): Observable<any[]> {
        var sparePartList = this.http
            .get(this.config.baseUrl + "Inventory/GetBySparePartName/" + term)
            .map((r: Response) => {
                return (r.json().length !== 0
                    ? r.json()
                    : [{ id: 0, Name_EN: "No Record Found" }]) as any[];
            });

        return sparePartList;
    }

    getSpareParts(): Observable<SparePart[]> {
        const url = this.config.baseUrl + "SparePart/Get";

        return this.http.get(url).map(data => {
            const results = <SparePart[]>data;
            return results;
        });
    }

    public getSparePart(id: number): Observable<any> {
        const url = this.config.baseUrl + "SparePart/Get/" + id;

        return this.http.get(url).map(
            data => {
                let result = <SparePart>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    addSparePart(model): Observable<SparePart> {
        model.rowStatusId = 1;
        model.createdByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "SparePart/Add";

        return this.http.post(url, model).map(
            data => {
                const results = <SparePart>data;
                return results;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    updateSparePart(model): Observable<SparePart> {
        model.rowStatusId = 1;
        model.modifiedByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "SparePart/Update";

        return this.http.put(url, model).map(
            data => {
                const results = <SparePart>data;
                return results;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    deleteSparePart(id) {
        let url = "{0}SparePart/Delete/{1}/{2}";

        url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
            .replace("{2}", this.config.currentUserId);
        
        return this.http.delete(url).map(
            data => {
                const results = <SparePart>data;
                return results;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    searchInventory(name) {
        const url = this.config.baseUrl + "Inventory/Search/" + name;

        return this.http.get(url).map(
            data => {
                const results = <SparePart>data;
                console.log(results);
                return results;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    // searchParams: { name: "", serialNo: "", category: ""}
    searchSpareParts(searchParams): Observable<SparePart[]> {

        const staticUrl = "{0}SparePart/Search?name={1}&serialNo={2}&category={3}";

        const url = staticUrl.replace("{0}", this.config.baseUrl).replace("{1}", searchParams.name)
            .replace("{2}", searchParams.serialNo).replace("{3}", searchParams.category);

        return this.http.get(url).map(data => {
            const results = <SparePart[]>data;
            return results;
        });
    }
}
