import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { External_PreRepairRequestComponent } from "./external_prerepairrequest/external_prerepairrequest.component";
import { External_RepairRequestDetailsComponent } from "./external_repairrequestdetails/external_repairrequestdetails.component";
import { External_SchRepairRequestComponent } from "./external_schrepairrequest/external_schrepairrequest.component";
import { HomeComponent } from "./home/home.component";
import { Internal_PreRepairRequestComponent } from "./internal_prerepairrequest/internal_prerepairrequest.component";
import { Internal_SchRepairRequestComponent } from "./internal_schrepairrequest/internal_schrepairrequest.component";
import { LoginComponent } from "./users/login/login.component";
import { NonApprovedRequestSparePartsComponent } from "./nonapprovedrequestspareparts/nonapprovedrequestspareparts.component";
import { NonApprovedRequestSparePartsDetailsComponent } from "./nonapprovedrequestsparepartsdetails/nonapprovedrequestsparepartsdetails.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { preJobCardComponent } from "./prejobcard/prejobcard.component";
import { PreventiveMaintenanceComponent } from "./preventivemaintenance/preventivemaintenance.component";
import { preventiveRepiarRequestComponent } from "./preventiveRepairRequest/preventiveRepiarRequest.component";
import { RegisterComponent } from "./Register/register.component";
import { RepairRequestsPendingComponent } from './repair-requests/pending/repair-requests-pending.component';
import { RepairRequestStatusListComponent } from "./repair-request-status/list/repair-request-status-list.component";
import { DailyReceiptReportComponent } from "./reports/dailyReceiptReport/dailyReceipt.component";
import { InternalPurchaseRequestComponent } from "./reports/internal-purchase-request/internal-purchase-request.component";
import { JobCardReportDetailsComponent } from "./reports/JobcardReport/viewReportDetails/viewReportDetails.component";
//import { MaterialRequisitionReportComponent } from "./reports/materialRequisitionReport/materialRequisition.component";
//import { SparePartsRequestReportComponent } from "./reports/sparePartsRequestReport/sparePartsRequest.component";
//import { toolsInspectionReportComponent } from "./reports/toolsInspection/toolsInspection.component";
import { vendorReportReportComponent } from "./reports/vendorReport/vendorReport.component";
import { RolesListComponent } from "./roles/list/roles-list.component";
import { ScheduledMaintenanceComponent } from "./scheduledmaintenance/scheduledmaintenance.component";
import { schJobCardComponent } from "./schjobcard/schjobcard.component";
import { SchJobCardDetailsComponent } from "./SchjobcardDetails/SchjobcardDetails.component";
import { sechedualRepiarRequestComponent } from "./sechedualRepiarRequest/sechedualRepiarRequest.component";
import { AuthGuard } from "./shared/services/AuthGuard.service";
import { TechniciansListComponent } from "./technicians/list/technicians-list.component";
import { TechniciansTabsComponent } from "./technicians/tabs/technicians-tabs.component";
import { UsersListComponent } from "./users/list/users-list.component";
import { VehicleTypesListComponent } from "./vehicle-types/list/vehicle-types-list.component";
import { VehiclesListComponent } from "./vehicles/list/vehicles-list.component";
import { VendorsListComponent } from "./vendors/list/vendors-list.component";
import { RepairRequestTypeListComponent } from "./repair-request-type/list/repair-request-type-list.component";
import { DeliveryOrderNewComponent } from "./spare-parts-delivery-orders/delivery-order-new/delivery-order-new.component";
import { DeliveryOrderListComponent } from "./spare-parts-delivery-orders/delivery-order-list/delivery-order-list.component";
import { UserProfileEditComponent } from "./users/profile-edit/user-profile-edit.component";
import { ServiceTypesListComponent } from "./service-types/list/service-types-list.component";
import { DriversListComponent } from "./drivers/list/drivers-list.component";
import { SparePartsListComponent } from "./spare-parts/list/spare-parts-list.component";
import { TechnicianManpowerComponent } from "./technicians/man-power/technician-man-power.component";
import { TechniciansWorkReportComponent } from "./technicians/work-report/technicians-work-report.component";
import { VehicleDetailsTabsComponent } from "./vehicles/details-tabs/vehicle-details-tabs.component";
import { RepairRequestsPendingDetailsComponent } from "./repair-requests/pending-details/repair-requests-pending-details.component";
import { RepairRequestsSparePartsReportComponent } from "./repair-request-spare-parts/report/repair-requests-spare-parts-report.component";
import { JobCardsListReportComponent } from "./job-cards/list-report/job-cards-list-report.component";

//import { JobCardComponent } from "./job-card/job-card.component";
//import { DashboardModule } from "./dashboard/dashboard.module";

// for authorization 'to insure user is for authorization 'to insure user is logging'

const routes: Routes = [
    { path: "login", component: LoginComponent },
    {
        path: "home",
        component: HomeComponent,
        children: [
            {
                path: "dashboard",
                component: DashboardComponent,
                //loadChildren: () => DashboardModule,
                canActivate: [AuthGuard]
            },
            {
                path: "register",
                component: RegisterComponent,
                canActivate: [AuthGuard]
            },
            //{
            //    path: "addDriver",
            //    component: AddDriverComponent,
            //    canActivate: [AuthGuard]
            //},
            {
                path: "technician_manpower",
                component: TechnicianManpowerComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "repair-requests/pending/list",
                component: RepairRequestsPendingComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "repair-requests/pending/details/:id",
                component: RepairRequestsPendingDetailsComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "repair-requests/spare-parts/pending/list",
                component: NonApprovedRequestSparePartsComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "repair-requests/spare-parts/pending/details/:id",
                component: NonApprovedRequestSparePartsDetailsComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "repair-requests/spare-parts/delivery-order/new",
                component: DeliveryOrderNewComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "repair-requests/spare-parts/delivery-order/list",
                component: DeliveryOrderListComponent,
                canActivate: [AuthGuard]
            },
            { path: "account/profile/edit", component: UserProfileEditComponent, canActivate: [AuthGuard] },
            { path: "vehicles", component: VehiclesListComponent, canActivate: [AuthGuard] },
            { path: "vehicles/details/:vehicleId", component: VehicleDetailsTabsComponent, canActivate: [AuthGuard] },
            { path: "notifications", component: NotificationsComponent, canActivate: [AuthGuard] },
            { path: "schjobcards", component: schJobCardComponent, canActivate: [AuthGuard] },
            { path: "schjobcardDetails/:id", component: SchJobCardDetailsComponent, canActivate: [AuthGuard] },
            { path: "prejobcards", component: preJobCardComponent, canActivate: [AuthGuard] },
            {
                path: "external_repairrequestdetails/:id",
                component: External_RepairRequestDetailsComponent,
                canActivate: [AuthGuard]
            },

            // Settings
            { path: "settings/users/list", component: UsersListComponent, canActivate: [AuthGuard] },
            { path: "settings/technicians/list", component: TechniciansListComponent, canActivate: [AuthGuard] },
            { path: "settings/technicians/tabs", component: TechniciansTabsComponent, canActivate: [AuthGuard] },
            { path: "settings/drivers/list", component: DriversListComponent, canActivate: [AuthGuard] },
            { path: "settings/service-types/list", component: ServiceTypesListComponent, canActivate: [AuthGuard] },
            { path: "settings/spare-parts/list", component: SparePartsListComponent, canActivate: [AuthGuard] },
            { path: "settings/vehicle-types/list", component: VehicleTypesListComponent, canActivate: [AuthGuard] },
            {
                path: "settings/repair-request-statuses/list",
                component: RepairRequestStatusListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "settings/repair-request-types/list",
                component: RepairRequestTypeListComponent,
                canActivate: [AuthGuard]
            },
            { path: "settings/vendors/list", component: VendorsListComponent, canActivate: [AuthGuard] },
            { path: "settings/roles/list", component: RolesListComponent, canActivate: [AuthGuard] },

            // Reports
            { path: "job-cards/report", component: JobCardsListReportComponent, canActivate: [AuthGuard] },
            { path: "job-cards/details/:id", component: JobCardReportDetailsComponent, canActivate: [AuthGuard] },
            {
                path: "job-cards/details/:vehicleId/:jobCardId",
                component: VehicleDetailsTabsComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "repair-requests/spare-parts/report",
                component: RepairRequestsSparePartsReportComponent,
                canActivate: [AuthGuard]
            },
            { path: "daily-services/report", component: DailyReceiptReportComponent, canActivate: [AuthGuard] },
            { path: "technicians/report", component: TechniciansWorkReportComponent, canActivate: [AuthGuard] },


            // [Mohammed.Osman]
            {
                path: "internal_prerepairrequests",
                component: Internal_PreRepairRequestComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "external_prerepairrequests",
                component: External_PreRepairRequestComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "internal_schrepairrequests",
                component: Internal_SchRepairRequestComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "external_schrepairrequests",
                component: External_SchRepairRequestComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "scheduledmaintenance",
                component: ScheduledMaintenanceComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "preventivemaintenance",
                component: PreventiveMaintenanceComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "sechedualRepiarRequest",
                component: sechedualRepiarRequestComponent,
                canActivate: [AuthGuard]
            },
            {
                path: "preventiveRepiarRequest",
                component: preventiveRepiarRequestComponent,
                canActivate: [AuthGuard]
            },
            //{
            //    path: "maintenanceReport",
            //    component: MaintenanceReportComponent,
            //    canActivate: [AuthGuard]
            //},
            //{
            //    path: "ospOrdersReport",
            //    component: ospOrdersReportComponent,
            //    canActivate: [AuthGuard]
            //},
            //{
            //    path: "toolsInspectionReport",
            //    component: toolsInspectionReportComponent,
            //    canActivate: [AuthGuard]
            //},
            //{
            //    path: "EquipmentReport",
            //    component: equipmentReportComponent,
            //    canActivate: [AuthGuard]
            //},

            //{
            //    path: "dailyGarageReport",
            //    component: dailyGarageReportComponent,
            //    canActivate: [AuthGuard]
            //},
            //{
            //    path: "material-requisition-report",
            //    component: MaterialRequisitionReportComponent,
            //    canActivate: [AuthGuard]
            //},
            {
                path: "internal-purchase-request-report",
                component: InternalPurchaseRequestComponent,
                canActivate: [AuthGuard]
            },
            //{
            //    path: "JobCardReport",
            //    component: EmptyJobCardReportComponent,
            //    canActivate: [AuthGuard]
            //},

            {
                path: "vendorsReport",
                component: vendorReportReportComponent,
                canActivate: [AuthGuard]
            },

            //{
            //    path: "addUpdateVehicle",
            //    component: VehicleAddEditComponent,
            //    canActivate: [AuthGuard]
            //},
            {
                path: "",
                redirectTo: "login",
                pathMatch: "full",
                canActivate: [AuthGuard]
            }
        ]
    },
    { path: "", redirectTo: "login", pathMatch: "full", canActivate: [AuthGuard] }
];

@NgModule({
    imports: [CommonModule, BrowserModule, RouterModule.forRoot(routes)],
    exports: []
})
export class AppRoutingModule { }
