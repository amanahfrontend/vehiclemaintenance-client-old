import { Component, OnInit } from "@angular/core";
import { JobCard } from "app/job-cards/models/job-card.model";
import { JobCardService } from "app/job-cards/services/job-card.service";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

@Component({
    selector: "app-schjobcard",
    templateUrl: "./schjobcard.component.html",
    styleUrls: ["./schjobcard.component.css"],
    providers: [JobCardService]
})
export class schJobCardComponent implements OnInit {
    jobCards: JobCard[];
    type: boolean;
    isActive: boolean = false;

    constructor(private jobCardservice: JobCardService) { }

    ngOnInit() {
        this.isActive = true;

        this.jobCardservice.getJobCards().subscribe(
            jobCardRes => {
                if (jobCardRes["isSucceeded"] === true) {
                    this.jobCards = jobCardRes["data"];
                    this.isActive = false;
                }
            });
    }
}
