import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { DeliveryOrderNewComponent } from "./delivery-order-new.component";

describe("DeliveryOrderNewComponent", () => {
    let component: DeliveryOrderNewComponent;
    let fixture: ComponentFixture<DeliveryOrderNewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DeliveryOrderNewComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DeliveryOrderNewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
