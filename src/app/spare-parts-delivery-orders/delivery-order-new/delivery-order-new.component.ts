import { Component, OnInit } from "@angular/core";
import { SparePartsDeliveryOrdersService } from "../services/spare-parts-delivery-orders.service";
import { CommonService } from "../../shared/services/common.service";
import { RepairRequestSparePartService } from "../../repair-request-spare-parts/services/repair-request-spare-part.service";
import { SparePartDeliveryOrder } from "../models/spare-part-delivery-order.model";
import { SparePartDeliveryOrderDetails } from "../models/spare-part-delivery-order.model";
import { DialogService } from "../../shared/services/dialog.service";

@Component({
    selector: "app-spare-part-new-delivery-order",
    templateUrl: "./delivery-order-new.component.html",
    providers: [SparePartsDeliveryOrdersService, RepairRequestSparePartService, CommonService]
})
export class DeliveryOrderNewComponent implements OnInit {
    showLoading: boolean = false;
    showNoDataFoundLabel: boolean = false;

    searchParams: {
        startDate: string,
        endDate: string,
        jobCardId: string;
    }

    sparePartsToDeliver: any = [];
    deliveryOrderRequestedBy: string = "";
    deliveryOrderId: number = 0;

    constructor(
        private sparePartsDeliveryOrdersService: SparePartsDeliveryOrdersService,
        private repairRequestSparePartService: RepairRequestSparePartService,
        private customDialogService: DialogService,
        private commonService: CommonService) {
    }

    ngOnInit() {

        this.searchParams = {
            startDate: "",
            endDate: "",
            jobCardId: ""
        };
    }

    clearSearch() {
        this.searchParams.startDate = "";
        this.searchParams.endDate = "";
        this.searchParams.jobCardId = "";
    }

    searchJobCardsUnDeliveredQty(): void {

        this.showLoading = true;

        let startDate: string;

        if (this.searchParams.startDate) {
            startDate = this.commonService.getDateAsString(this.searchParams.startDate);
        } else {
            startDate = null;
        }

        let endDate: string;

        if (this.searchParams.endDate) {
            endDate = this.commonService.getDateAsString(this.searchParams.endDate);
        } else {
            endDate = null;
        }

        const jobCardId = this.searchParams.jobCardId;

        this.repairRequestSparePartService.searchJobCardsUnDeliveredQty(
            startDate, endDate, jobCardId).subscribe(
                responseResult => {

                    if (responseResult["isSucceeded"] === true) {
                        this.sparePartsToDeliver = responseResult["data"];
                        this.showLoading = false;

                        this.showNoDataFoundLabel = this.sparePartsToDeliver.length === 0;
                    } else {
                        console.log(responseResult.message);
                    }
                },
                () => {
                    this.showLoading = false;
                }
            );
    }

    saveDeliveryOrder(): void {
        const isValidData = this.validateDeliveryOrderDetails();

        let errorMessage: string;

        if (!isValidData) {
            errorMessage = "Found at leaset one invalid quantity either exceeds the maximum allowed or zero.";
            this.customDialogService.alert("Fail", errorMessage, false);

            return;
        }

        const sparePartDeliveryOrderDetails = this.prepareDeliveryOrderDetails();

        if (sparePartDeliveryOrderDetails.length === 0) {
            errorMessage = "Please fill at least one quantity of spare part to deliver.";
            this.customDialogService.alert("Fail", errorMessage, false);

            return;
        }

        if (this.deliveryOrderRequestedBy === "") {
            errorMessage = "Please fill in the field 'Requested by'.";
            this.customDialogService.alert("Fail", errorMessage, false);

            return;
        }

        const model: SparePartDeliveryOrder = {
            requestedBy: this.deliveryOrderRequestedBy,
            sparePartDeliveryOrderDetails: sparePartDeliveryOrderDetails
        };

        this.showLoading = true;

        this.sparePartsDeliveryOrdersService.addNewDeliveryOrder(model).subscribe(
            responseResult => {

                if (responseResult && responseResult.isSucceeded === true) {

                    console.log(responseResult["data"]);
                    this.deliveryOrderId = responseResult["data"].id;
                    this.deliveryOrderRequestedBy = "";

                    this.searchJobCardsUnDeliveredQty();

                    this.showLoading = false;
                } else {
                    console.log(responseResult.message);
                }
            },
            (error) => {
                console.log(error);
                this.showLoading = false;
            }
        );
    }

    validateDeliveryOrderDetails(): boolean {

        for (let i = 0; i < this.sparePartsToDeliver.length; i++) {
            const sparePart = this.sparePartsToDeliver[i];

            if (sparePart.deliveredQuantity === undefined) {
                sparePart.deliveredQuantity = 0;
            }

            if (sparePart.quantityToDeliver !== undefined) {
                const totalDeliveredQty = sparePart.deliveredQuantity + sparePart.quantityToDeliver;
                
                const validInput: boolean = sparePart.quantityToDeliver > 0 &&
                    totalDeliveredQty <= sparePart.quantity;

                if (!validInput) {

                    return false;
                }
            }
        }

        return true;
    }

    prepareDeliveryOrderDetails(): SparePartDeliveryOrderDetails[] {

        const sparePartDeliveryOrderDetails: SparePartDeliveryOrderDetails[] = [];

        for (let i = 0; i < this.sparePartsToDeliver.length; i++) {
            const sparePart = this.sparePartsToDeliver[i];

            const totalDeliveredQty = sparePart.deliveredQuantity + sparePart.quantityToDeliver;

            const validInput: boolean = sparePart.quantityToDeliver > 0 &&
                totalDeliveredQty <= sparePart.quantity;

            if (validInput) {

                sparePartDeliveryOrderDetails.push({
                    repairRequestSparePartId: sparePart.id,
                    sparePartId: sparePart.sparePartId,
                    deliveredQty: sparePart.quantityToDeliver
                });
            }
        }

        return sparePartDeliveryOrderDetails;
    }

    printDeliveryOrderReport() {
        // 6 =  MaterialRequisitionReport.rdl
        this.commonService.print2(6, `deliveryOrderId=${this.deliveryOrderId}`);
    }

    printEmptyMaterialRequisitionReport(): void {
        // 6 =  MaterialRequisitionReport.rdl
        this.commonService.print2(6, "deliveryOrderId=0");
    }
}
