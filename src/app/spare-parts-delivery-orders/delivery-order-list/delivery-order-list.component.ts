import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../shared/services/common.service";
import { DeliveryOrderSearchParams, SparePartDeliveryOrder } from "../models/spare-part-delivery-order.model";
import { SparePartsDeliveryOrdersService } from "../services/spare-parts-delivery-orders.service";

@Component({
    selector: "app-delivery-order-list",
    templateUrl: "./delivery-order-list.component.html",
    providers: [SparePartsDeliveryOrdersService, CommonService]
})
export class DeliveryOrderListComponent implements OnInit {

    showLoading: boolean = false;
    showNoDataFoundLabel: boolean = false;

    searchParams: DeliveryOrderSearchParams = new DeliveryOrderSearchParams();
    deliveryOrders: SparePartDeliveryOrder[];

    constructor(
        private sparePartsDeliveryOrdersService: SparePartsDeliveryOrdersService,
        private commonService: CommonService) {
    }

    ngOnInit() {
    }

    searchDeliveryOrders() {

        this.searchParams.startDate = this.commonService.getDateAsString(this.searchParams.startDate);
        this.searchParams.endDate = this.commonService.getDateAsString(this.searchParams.endDate);

        this.sparePartsDeliveryOrdersService.searchDeliveryOrders(this.searchParams).subscribe(
            responseResult => {

                if (responseResult && responseResult["isSucceeded"]) {
                    this.deliveryOrders = responseResult["data"];
                }
            });
    }

    clearSearch() {
        this.searchParams.clearValues();
    }

    printDeliveryOrderReport(deliveryOrderId: number) {
        // 6 =  MaterialRequisitionReport.rdl
        this.commonService.print2(6, `deliveryOrderId=${deliveryOrderId}`);
    }
}
