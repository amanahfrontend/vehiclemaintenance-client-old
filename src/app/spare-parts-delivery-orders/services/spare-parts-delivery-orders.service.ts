import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { WebService } from "../../shared/services/webService";
import { DeliveryOrderSearchParams, SparePartDeliveryOrder } from "../models/spare-part-delivery-order.model";

@Injectable()
export class SparePartsDeliveryOrdersService {

    constructor(private http: HttpClient, private config: WebService) { }

    searchDeliveryOrders(searchParams: DeliveryOrderSearchParams): Observable<SparePartDeliveryOrder[]> {
        let url = "{0}SparePartDeliveryOrder/search?deliveryOrderId={1}&startDate={2}&endDate={3}";

        url = url.replace("{0}", this.config.baseUrl).replace("{1}", searchParams.deliveryOrderId)
            .replace("{2}", searchParams.startDate).replace("{3}", searchParams.endDate);

        return this.http.get(url).map(data => {
            const result = <SparePartDeliveryOrder[]>data;
            return result;
        });
    }

    addNewDeliveryOrder(model: SparePartDeliveryOrder): Observable<any> {
        model.createdByUserId = this.config.currentUserId;
        model.rowStatusId = 1;

        const url = this.config.baseUrl + "SparePartDeliveryOrder/Add";

        return this.http.post(url, model).map(
            data => {
                const result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            }
        );
    }
}
