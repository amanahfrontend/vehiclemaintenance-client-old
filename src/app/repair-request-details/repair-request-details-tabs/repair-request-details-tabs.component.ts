import { MdlDialogService } from '@angular-mdl/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RepairRequestSparePartsListComponent } from "../../repair-request-spare-parts/list/repair-request-spare-parts-list.component";
import { RepairRequestService } from "../../repair-requests/services/repair-request.service";
import { IRepairRequest_Technician } from "../../shared/Interfaces/IRepairRequest_Technician.interface";
import { ISparePartsRequest } from "../../shared/Interfaces/ISparePartsRequest.interface";
import { IUsedMaterials } from "../../shared/Interfaces/IUsedMaterials.interface";
import { CommonVariables } from "../../shared/services/common-variables.service";
import { CommonService } from "../../shared/services/common.service";
import { DialogService } from "../../shared/services/dialog.service";
import { RepairRequestSparePartService } from "../../repair-request-spare-parts/services/repair-request-spare-part.service";
import { SparePartStatusService } from "../../shared/services/sparepartstatus.service";
import { UsedMaterialsService } from "../../shared/services/usedmaterials.service";
import { SparePart } from "../../spare-parts/models/spare-part.model";
import { SparePartService } from "../../spare-parts/services/spare-part.service";
import { Technician } from "../../technicians/models/technician.model";
import { TechnicianService } from "../../technicians/services/technician.service";

@Component({
    selector: 'app-repair-request-details-tabs',
    templateUrl: './repair-request-details-tabs.component.html',
    styleUrls: ['./repair-request-details-tabs.component.css'],
    providers: [RepairRequestService, TechnicianService, RepairRequestSparePartService,
        SparePartService, UsedMaterialsService, CommonVariables,
        SparePartStatusService, CommonService]
})
export class RepairRequestDetailsTabsComponent implements OnInit {

    model: any = {};
    repairRequestTechnicians: IRepairRequest_Technician[];
    requestSpareParts: ISparePartsRequest[];
    isActive: boolean = false;
    repairRequestId: number;
    userRole: string;
    usedMaterials: IUsedMaterials[];
    startTime: any = new Date();
    visible = false;
    visibleAnimate = false;
    technicians: Technician[];
    spareParts: SparePart[];
    date: any = new Date();

    isCollapsedSpare = false;
    isCollapsedMaterials = false;

    modelTech: any = {};
    type: boolean;
    startTimeTech: any = new Date();
    modelMaterial: any = {};
    sparePartStatuses: any;
    vehicles: any;
    transferFrom: string = "";
    allowEdit: boolean;

    editModeSparePart: boolean = false;
    lblAddEditRequestSparePartText: string = "Add spare part";
    btnAddUpdateRequestSparePartText: string = "Add spare part";

    editModeMaterial: boolean = false;
    lblAddEditRequestMaterialText: string = "Add Lubricants";
    btnAddUpdateRequestMaterialText: string = "Add Lubricants";

    @ViewChild("pendingRepairRequestSpareParts") pendingRepairRequestSpareParts: RepairRequestSparePartsListComponent;
    @ViewChild("activeRepairRequestSpareParts") activeRepairRequestSpareParts: RepairRequestSparePartsListComponent;

    constructor(
        private commonS: CommonVariables,
        private dialogService: MdlDialogService,
        private repairRequestSparePartService: RepairRequestSparePartService,
        private technicianService: TechnicianService,
        private sparePartService: SparePartService,
        private repairRequestService: RepairRequestService,
        private customDialogService: DialogService,
        private sparePartStatusService: SparePartStatusService,
        private repairRequestMaterialService: UsedMaterialsService,
        private route: ActivatedRoute,
        private commonService: CommonService
    ) {
        window.scroll(0, 0);
    }

    ngOnInit() {
        window.scroll(0, 0);

        this.userRole = sessionStorage.getItem("userRole");

        this.route.params.subscribe(params => {
            this.repairRequestId = +params["id"];
        });

        // get Technicians.
        this.technicianService.getTechnicians().subscribe(technicianRes => {

            if (technicianRes["isSucceeded"] === true) {
                this.technicians = technicianRes["data"];
            }
        });

        // get spare Parts
        this.sparePartService.getSpareParts().subscribe(
            sparePartsRes => {
                if (sparePartsRes["isSucceeded"] === true) {
                    this.spareParts = sparePartsRes["data"];
                }
            });

        // get Spare Part Status
        this.sparePartStatusService.getSparePartStatus().subscribe(
            sparepartstatusRes => {
                if (sparepartstatusRes["isSucceeded"] === true) {
                    this.sparePartStatuses = sparepartstatusRes["data"];
                }
            });
    }

    show(): void {
        this.visible = true;
        setTimeout(() => (this.visibleAnimate = true), 100);
    }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => (this.visible = false), 300);
    }

    showRepairTabs(repairRequestId, allowEdit: boolean): void {
        window.scroll(0, 0);

        this.allowEdit = allowEdit;

        this.commonS.repairId = repairRequestId;
        this.repairRequestId = repairRequestId;

        //this.getRepairRequestSparParts(repairRequestId);
        this.getRepairRequestMaterials(repairRequestId);

        this.visible = true;
        setTimeout(() => (this.visibleAnimate = true), 100);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    delete() {
        const question = "Are you sure, you want to delete this repair request?";
        let result = this.dialogService.confirm(question, "No", "Yes");

        // if you need both answers
        result.subscribe(() => {

            this.repairRequestService.deleteRepairRequest(this.repairRequestId).subscribe(
                repairRequestRes => {

                    if (repairRequestRes["isSucceeded"] === true) {
                        //let result = this.dialogService.alert("deleted Successfully");
                        //result.subscribe(() => location.reload());
                    } else {
                        this.dialogService.alert("deleting Failed");
                    }
                });
        });
    }

    currentUserIsManager() {
        return (this.userRole === "Manager");
    }

    // Spare Parts ...................................................

    openAddModeRepairRequestSparePart(repairRequestId): void {

        this.model = {
            repairRequestId: repairRequestId,
            technicianStartDate: new Date(),
            technicianEndDate: new Date()
        };

        this.commonS.repairId = repairRequestId;

        this.lblAddEditRequestSparePartText = "Add spare part";
        this.btnAddUpdateRequestSparePartText = "Add spare part";
        this.isCollapsedSpare = !this.isCollapsedSpare;

        this.editModeSparePart = false;
    }

    openEditModeRepairRequestSparePart(repairRequestSparePart: any) {
        Object.assign(this.model, repairRequestSparePart);

        let repairRequestId = repairRequestSparePart.repairRequestId;
        this.model.repairRequestId = repairRequestId;
        this.commonS.repairId = repairRequestId;

        this.lblAddEditRequestSparePartText = "Edit spare part";
        this.btnAddUpdateRequestSparePartText = "Update spare part";
        this.isCollapsedSpare = !this.isCollapsedSpare;
        this.editModeSparePart = true;
    }

    validateBeforeSavingRequestSparePart(): boolean {

        if (!this.model.technicianStartDate) {
            this.dialogService.alert("Technician start date is required");
            return false;
        }

        if (!this.model.technicianEndDate) {
            this.dialogService.alert("Technician end date is required");
            return false;
        }

        if (this.commonService.isDateObjIsString(this.model.technicianStartDate)) {

            this.model.technicianStartDate = this.commonService.convertStringToDate(
                this.model.technicianStartDate);

            this.model.technicianEndDate = this.commonService.convertStringToDate(
                this.model.technicianEndDate);
        }

        // Ignore seconds.
        this.model.technicianStartDate.setSeconds(0);
        this.model.technicianEndDate.setSeconds(0);

        if (this.model.technicianEndDate.getTime() <= this.model.technicianStartDate.getTime()) {
            this.dialogService.alert("Technician End date should be greater than technician start date.");
            return false;
        }

        return true;
    }

    saveRequestSparePart() {

        const isValid = this.validateBeforeSavingRequestSparePart();

        if (!isValid) {
            return;
        }

        if (this.model.technicianStartDate) {
            this.model.technicianStartDate = this.commonService.getDateTimeAsString(this.model.technicianStartDate);
        }

        if (this.model.technicianEndDate) {
            this.model.technicianEndDate = this.commonService.getDateTimeAsString(this.model.technicianEndDate);
        }

        const saveRepairRequestSparePart = this.model.id > 0
            ? this.repairRequestSparePartService.updateRepairRequestSparePart(this.model)
            : this.repairRequestSparePartService.addRepairRequestSparePart(this.model);

        saveRepairRequestSparePart.subscribe(
            responseResult => {
                if (responseResult && responseResult.isSucceeded === true) {

                    // Show message if the spare part needs an approval.
                    if (responseResult.data.rowStatusId === 0) {
                        this.dialogService.alert("Spare part added successfully but needs an approval. Spare part now is pending.");
                        this.pendingRepairRequestSpareParts.repairRequestId = this.commonS.repairId;
                    } else {
                        this.activeRepairRequestSpareParts.repairRequestId = this.commonS.repairId;
                    }

                    if (!this.editModeSparePart) {
                        // If add mode of Repair request spare part, clear the model.
                        this.model = {
                            repairRequestId: this.commonS.repairId,
                            technicianStartDate: new Date(),
                            technicianEndDate: new Date()
                        };
                    } else {
                        this.dialogService.alert("Record updated successfully.");
                    }

                } else {
                    this.dialogService.alert("Something wrong. " + responseResult.message);
                }
            });
    }

    // Repair Request Materials ..........................

    openAddModeRepairRequestMaterial(repairRequestId): void {
        this.modelMaterial = {};
        this.commonS.repairId = repairRequestId;
        this.modelMaterial.repairRequestId = repairRequestId;

        this.lblAddEditRequestMaterialText = "Add lubricants";
        this.btnAddUpdateRequestMaterialText = "Add lubricants";
        this.isCollapsedMaterials = !this.isCollapsedMaterials;
        this.editModeMaterial = false;
    }

    openEditModeRepairRequestMaterial(repairRequestMaterial: any) {
        Object.assign(this.modelMaterial, repairRequestMaterial);

        this.lblAddEditRequestMaterialText = "Edit lubricants";
        this.btnAddUpdateRequestMaterialText = "Update lubricants";
        this.isCollapsedMaterials = !this.isCollapsedMaterials;
        this.editModeMaterial = true;
    }

    getRepairRequestMaterials(repairRequestId) {
        this.repairRequestMaterialService.getByRepairRequestId(repairRequestId).subscribe(
            responseResult => {
                this.isActive = true;

                if (responseResult && responseResult["isSucceeded"] === true) {
                    this.usedMaterials = responseResult["data"];
                    this.isActive = false;
                }
            });
    }

    saveRepairRequestMaterial() {

        const saveRepairRequestMaterial = this.modelMaterial.id > 0
            ? this.repairRequestMaterialService.updateRepairRequestMaterial(this.modelMaterial)
            : this.repairRequestMaterialService.addRepairRequestMaterial(this.modelMaterial);

        saveRepairRequestMaterial.subscribe(
            responseResult => {
                if (responseResult["isSucceeded"] === true) {

                    if (!this.editModeMaterial) {
                        // If add mode of Repair request material, clear the model.

                        this.modelMaterial = {
                            repairRequestId: this.commonS.repairId
                        };
                    } else {
                        this.dialogService.alert("Record updated successfully.");
                    }

                    this.getRepairRequestMaterials(this.commonS.repairId);
                } else {
                    this.customDialogService.alert("Fail", "Something wrong.", false);
                    console.log(JSON.stringify(responseResult.message));
                }
            });
    }

    deleteRepairRequestMaterial(repairRequestMaterialId: number) {
        const question = "Are you sure you want to delete this material of the repair request?";
        const result = this.dialogService.confirm(question, "No", "Yes");

        // if you need both answers
        result.subscribe(() => {

            this.repairRequestMaterialService.deleteRepairRequestMaterial(repairRequestMaterialId).subscribe(
                responseResult => {

                    if (responseResult["isSucceeded"] === true) {
                        this.getRepairRequestMaterials(this.commonS.repairId);
                    } else {
                        this.dialogService.alert("Deleting failed. " + responseResult.message);
                    }
                });
        });
    }
}
