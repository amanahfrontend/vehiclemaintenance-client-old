import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestDetailsTabsComponent } from './repair-request-details-tabs.component';

describe('RepairRequestDetailsTabsComponent', () => {
  let component: RepairRequestDetailsTabsComponent;
  let fixture: ComponentFixture<RepairRequestDetailsTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestDetailsTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestDetailsTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
