import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { JobCardStatus } from "app/job-cards/models/job-card-status.model";
import { JobCardStatusService } from "app/job-cards/services/job-card-status.service";
import { JobCardService } from "app/job-cards/services/job-card.service";
import { Vehicle } from "app/vehicles/models/vehicle.model";
import { VehicleService } from "app/vehicles/services/vehicle.service";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DriverService } from "../drivers/services/driver.service";
import { ServiceTypeService } from "../service-types/services/service-type.service";
import { IDriver } from "../shared/Interfaces/IDriver.interface";
import { IJobCard_Technician } from "../shared/Interfaces/IJobCard_Technician.interface";
import { IServiceType } from "../shared/Interfaces/IServiceType.interface";
import { IUsedMaterials } from "../shared/Interfaces/IUsedMaterials.interface";
import { JobCard_TechnicianService } from "../shared/services/jobcard_technician.service";
import { UsedMaterialsService } from "../shared/services/usedmaterials.service";

@Component({
    selector: "app-updateprejobcard",
    templateUrl: "./updateprejobcard.component.html",
    styleUrls: ["./updateprejobcard.component.css"],
    providers: [JobCardService, JobCardStatusService, DriverService, ServiceTypeService, VehicleService]
})
export class UpdatePreJobCardComponent implements OnInit {
    router: Router;
    public visible = false;
    public visibleAnimate = false;
    model: any = {};
    jobcard_technicians: IJobCard_Technician[];
    usedmaterials: IUsedMaterials[];
    drivers: IDriver[];
    serviceTypes: IServiceType[];
    vehicles: Vehicle[];
    status: JobCardStatus;
    type: boolean;

    constructor(_router: Router, private dialogService: MdlDialogService,
        private jobcardservice: JobCardService,
        private serviceTypeservice: ServiceTypeService,
        private vehicleService: VehicleService,
        private driverservice: DriverService,
        private usedmaterialservice: UsedMaterialsService,
        private jobcard_technicianservice: JobCard_TechnicianService,

        private statusservice: JobCardStatusService) {
        this.router = _router;
    }

    ngOnInit() {

        ////////// get Status ///////////////
        this.statusservice.getJobCardStatuses().subscribe(
            statusRes => {
                if (statusRes["isSucceeded"] === true) {
                    this.status = statusRes["data"];
                }
                return this.serviceTypes;
            });

        ////////// get ServiceTypes ///////////////
        this.serviceTypeservice.getServiceTypes().subscribe(
            serviceTypeRes => {
                if (serviceTypeRes["isSucceeded"] === true) {
                    this.serviceTypes = serviceTypeRes["data"];
                }
            });

        ////////// get Vehicles ///////////////
        this.vehicleService.getVehicles().subscribe(vehicleRes => {
            if (vehicleRes["isSucceeded"] === true) {
                this.vehicles = vehicleRes["data"];
            }

        });

        ////////// get Drivers ///////////////
        this.driverservice.getDrivers().subscribe(driverRes => {
            if (driverRes["isSucceeded"] === true) {
                this.drivers = driverRes["data"];
            }

        });
    }

    public shows(model): void {
        this.model = model;
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }


    updateJobCard() {

        if (this.model.fK_Status_ID == 3) {
            ////////// get Used Materials ///////////////
            this.usedmaterialservice.getByjobcardId(this.model.id).subscribe(
                usedmaterialRes => {
                    if (usedmaterialRes["isSucceeded"] === true) {
                        //let result = this.dialogService.alert('Updated Successfully');
                        //result.subscribe(() => console.log('success'));

                        this.usedmaterials = usedmaterialRes["data"];
                        if (usedmaterialRes["data"].length > 0) {
                            ////////// get Jobcard_Technicians ///////////////
                            this.jobcard_technicianservice.getByjobcardId(this.model.id).subscribe(
                                jobcard_technicianRes => {
                                    if (jobcard_technicianRes["isSucceeded"] === true) {
                                        //let result = this.dialogService.alert('Updated Successfully');
                                        //result.subscribe(() => console.log('success'));
                                        this.jobcard_technicians = jobcard_technicianRes["data"];
                                        if (jobcard_technicianRes["data"].length > 0) {
                                            this.jobcardservice.updateJobCard(this.model).subscribe(
                                                jobcardRes => {
                                                    if (jobcardRes["isSucceeded"] === true) {
                                                        //let result = this.dialogService.alert('Updated Successfully');
                                                        //result.subscribe(() => console.log('success'));
                                                        this.visibleAnimate = false;
                                                        setTimeout(() => this.visible = false, 300);
                                                        this.router.navigateByUrl("/home/prejobcards");
                                                    }
                                                });
                                        }
                                        else {
                                            this.dialogService.alert("Please add the Technicians who worked for this job card.")
                                        }

                                    }
                                    return this.jobcard_technicians;
                                });

                        }
                        else {
                            this.dialogService.alert("Please add the Used Matrials for this job card.");
                        }
                    }
                    return this.usedmaterials;
                });
        }
        else {

            this.jobcardservice.updateJobCard(this.model).subscribe(jobcardRes => {
                if (jobcardRes["isSucceeded"] === true) {
                    //let result = this.dialogService.alert('Updated Successfully');
                    //result.subscribe(() => console.log('success'));
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);
                    this.router.navigateByUrl("/home/prejobcards");
                }
                else {
                    this.dialogService.alert("some thing wrong");
                }
            });
        }
    }
}
