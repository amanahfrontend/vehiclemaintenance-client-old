import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { IVendor } from "../models/IVendor.interface";
import { WebService } from "../../shared/services/webService";

@Injectable()
export class VendorsService {

    constructor(private http: HttpClient, private config: WebService) { }

    getVendors(): Observable<any[]> {
        const url = this.config.baseUrl + "Vendor/Get";

        return this.http.get(url).map(data => {
            const result = <any[]>data;
            return result;
        });
    }

    addVendor(model): Observable<IVendor> {
        model.rowStatusId = 1;
        model.createdByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "Vendor/Add";

        return this.http.post(url, model).map(
            data => {
                const result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    updateVendor(model): Observable<IVendor> {
        model.rowStatusId = 1;
        model.modifiedByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "Vendor/Update";

        return this.http.put(url, model).map(
            data => {
                const result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    deleteVendor(id) {
        
        let url = "{0}Vendor/Delete/{1}/{2}";

        url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
            .replace("{2}", this.config.currentUserId);

        return this.http.delete(url).map(
            data => {
                const result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            });
    }

    vendorReport(id): Observable<any> {
        const url = this.config.baseUrl + "Vendor/Get/" + id;
        return this.http.get(url).map(response => response);
    }
}
