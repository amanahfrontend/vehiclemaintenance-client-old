export interface IVendor {
    id: number;
    name: string;
    address: string;
}