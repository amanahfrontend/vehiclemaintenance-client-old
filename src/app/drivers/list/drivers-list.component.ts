import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DriverAddComponent } from "../add/driver-add.component";
import { DialogService } from "../../shared/services/dialog.service";
import { DriverService } from "../services/driver.service";

@Component({
    selector: "app-drivers-list",
    templateUrl: "./drivers-list.component.html",
    styleUrls: ["./drivers-list.component.scss"],
    providers: [DriverService]
})
export class DriversListComponent implements OnInit {
    drivers: any = [];
    isActive: boolean = false;
    query: string = "";

    constructor(private driverService: DriverService,
        private customDialogService: DialogService,
        public dialog: MatDialog) {
    }

    ngOnInit() {
        this.isActive = true;
        this.getAllDriver();
    }

    getAllDriver() {
        this.driverService.getDrivers().subscribe(driverRes => {

            if (driverRes["isSucceeded"] === true) {
                this.drivers = driverRes["data"];
                this.isActive = false;
            }

        });
    }

    openAddEditDriverDialog(id: number): void {

        const dlgOptions = {
            width: "650px",
            height: "550px",
            data: { id: id }
        };

        let dialogRef = this.dialog.open(DriverAddComponent, dlgOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.getAllDriver();
        });
    }

    getDriverBySearch() {
        this.driverService.getDriversWithSearch(this.query).subscribe(
            driverRes => {

                if (driverRes["isSucceeded"] === true) {
                    this.drivers = driverRes["data"];
                    this.isActive = false;
                }
            });
    }

    searchDriver() {

        if (this.query) {
            this.getDriverBySearch();
        } else {
            this.getAllDriver();
        }
    }

    deleteDriver(id) {
        const question = "Do you really want to delete this driver?";
        const result = this.customDialogService.confirm("Confirm", question, true);

        result.subscribe(
            (res) => {
                if (res) {

                    this.driverService.deleteDriver(id).subscribe(
                        driverRes => {

                            if (driverRes["isSucceeded"] === true) {

                                this.searchDriver();
                            }
                        });
                }
            },
            () => {
                console.log("declined");
            }
        );
    }
}
