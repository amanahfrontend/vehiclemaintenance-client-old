import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IFile } from "../../shared/Interfaces/IFile.interface";
import { IRepairRequest } from "../../shared/Interfaces/IRepairRequest.interface";
import { ISparePartsRequest } from "../../shared/Interfaces/ISparePartsRequest.interface";
import { WebService } from "../../shared/services/webService";

@Injectable()
export class RepairRequestService {
    constructor(private http: HttpClient, private config: WebService) { }

    getRepairRequests(): Observable<any> {
        const url = this.config.baseUrl + "RepairRequest/Get";

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    getByMaintnanceType(type): Observable<IRepairRequest[]> {
        const url = this.config.baseUrl + "RepairRequest/GetByMaintenanceType/" + type;

        return this.http.get(url).map(data => {
            let result = <IRepairRequest[]>data;
            return result;
        });
    }

    getByExternalandMaintenanceType(external, type): Observable<IRepairRequest[]> {
        const url = this.config.baseUrl + "RepairRequest/GetByExternalandMaintenanceType?external=" + external + "&type=" + type;

        return this.http.get(url).map(data => {
            let result = <IRepairRequest[]>data;
            return result;
        });
    }

    addRepairRequest(model): Observable<any> {
        model.rowStatusId = 1;
        model.createdByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "RepairRequest/Add";

        return this.http.post(url, model).map(
            data => {
                let result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            });
    }

    getRepairRequestById(id): Observable<any> {
        const url = this.config.baseUrl + "RepairRequest/GetRepairRequest/" + id;

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    getByJobCardId(id): Observable<any> {
        const url = this.config.baseUrl + "RepairRequest/GetByJobCardID/" + id;

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    deleteRepairRequest(id): Observable<any> {
        let url = "{0}RepairRequest/Delete/{1}/{2}";
        url = url.replace("{0}", this.config.baseUrl).replace("{1}", id).replace("{2}", this.config.currentUserId);

        return this.http.delete(url).map(
            data => {
                let result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            });
    }

    updateRepairRequest(model): Observable<any> {
        model.rowStatusId = 1;
        model.modifiedByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "RepairRequest/Update";

        return this.http.put(url, model).map(
            data => {
                const result = <any>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error));
            });
    }

    getPendingRepairRequests(): Observable<any> {
        const url = this.config.baseUrl + "RepairRequest/GetPendingRepairRequests";

        return this.http.get(url).map(response => response);
    }

    vendorReportRepairRequest(id): Observable<IFile[]> {
        const url = this.config.baseUrl + "File/GetByRepairRequestIDandChoosedVendor/" + id;

        return this.http.get(url).map(data => {
            let result = <IFile[]>data;
            return result;
        });
    }

    sparPartsReportRepairRequest(id): Observable<any> {
        const url = this.config.baseUrl + "SparePartsRequest/GetByRepairRequestID/" + id;

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    materialRequestionReport(id): Observable<ISparePartsRequest[]> {
        const url = this.config.baseUrl + "SparePartsRequest/GetByRepairRequestID2/" + id;

        return this.http.get(url).map(data => {
            let result = <ISparePartsRequest[]>data;
            return result;
        });
    }

    getAllInternalSpares(): Observable<ISparePartsRequest[]> {
        const url = this.config.baseUrl + "SparePartsRequest/GetAllInternal";

        return this.http.get(url).map(data => {
            let result = <ISparePartsRequest[]>data;
            return result;
        });
    }

    payRequestReport(sparePartRequestId): Observable<any> {
        const url = this.config.baseUrl + "SparePartsRequest/GetById/" + sparePartRequestId;

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }

    getRepaireRequestWithSearch(query): Observable<any> {
        const urlStatic = "RepairRequest/SearchByID_JobcardID?word={0}";
        const url = this.config.baseUrl + urlStatic.replace('{0}', query);

        return this.http.get(url).map(data => {
            let result = <any>data;
            return result;
        });
    }
}
