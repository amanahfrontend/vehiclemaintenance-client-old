﻿import { UploadedFile } from "../../file-uploader/uploaded-file.model";
import { BaseEntity } from "../../shared/models/base-entity.model";

export class RepairRequestVendor extends BaseEntity {
    repairRequestId: number;
    vendorId: number;
    files: UploadedFile[];
}