export interface IRepairRequestStatus {
    id: string;
    nameAr: string;
    nameEn: string;
}