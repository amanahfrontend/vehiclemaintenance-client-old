﻿export enum EnumRepairRequestStatus {
    NeedApproval = 1,
    UnderQuotation = 2,
    Closed = 4,
    InProgress = 5
}