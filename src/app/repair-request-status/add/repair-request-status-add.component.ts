import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { DialogService } from "../../shared/services/dialog.service";
import { RepairRequestStatusService } from "../services/repair-request-status.service";

@Component({
    selector: "app-repair-request-status-add",
    templateUrl: "./repair-request-status-add.component.html",
    providers: [RepairRequestStatusService]
})
export class RepairRequestStatusAddComponent implements OnInit {
    router: Router;
    public visible = false;
    public visibleAnimate = false;
    model: any = {};

    constructor(
        private repairRequestStatusService: RepairRequestStatusService,
        private customDialogService: DialogService,
        public dialogRef: MatDialogRef<RepairRequestStatusAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() { }

    show(): void {
        this.model = {};
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    hide(): void {
        this.model = {};
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    saveRepairRequestStatus() {

        this.repairRequestStatusService.addRepairRequestStatus(this.model).subscribe(
            repairrequeststatusRes => {
                if (repairrequeststatusRes["isSucceeded"] === true) {
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);
                    this.customDialogService.alert("Done", "added successfully", false);
                    this.dialogRef.close();
                } else {
                    this.customDialogService.alert("Fail", "some thing wrong", false);
                }
            });
    }

    closeDialog(): void {
        this.dialogRef.close();
    }
}

