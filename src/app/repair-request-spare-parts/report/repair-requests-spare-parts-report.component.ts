import { Component, OnInit, ViewChild } from "@angular/core";
import { JobCardRepairRequestSearchCriteriaComponent } from "app/job-card-repair-request-search-criteria/job-card-repair-request-search-criteria.component";
import { CommonService } from "app/shared/services/common.service";
import { SparePartService } from "../../spare-parts/services/spare-part.service";
import { RepairRequestSparePartSearchParams } from "../models/repair-request-spare-part.model";
import { RepairRequestSparePartService } from "../services/repair-request-spare-part.service";

@Component({
    selector: "app-repair-requests-spare-parts-report",
    templateUrl: "./repair-requests-spare-parts-report.component.html",
    styleUrls: ["./repair-requests-spare-parts-report.component.css"],
    providers: [SparePartService, RepairRequestSparePartService, CommonService]
})
export class RepairRequestsSparePartsReportComponent implements OnInit {

    @ViewChild("ctrlSearchCriteria")
    ctrlSearchCriteria: JobCardRepairRequestSearchCriteriaComponent;

    showLoading: boolean = false;
    spareParts: any = [];
    repairRequestSpareParts: any = [];
    showNoDataFoundLabel: boolean = false;

    filteredOptions: any = [];

    searchParams = new RepairRequestSparePartSearchParams();

    constructor(
        private readonly sparePartService: SparePartService,
        private readonly repairRequestSparePartService: RepairRequestSparePartService,
        private readonly commonService: CommonService) {
    }

    ngOnInit() {

        this.getSparParts();
    }

    getSparParts() {
        this.showLoading = true;

        this.sparePartService.getSpareParts().subscribe(
            sparePartsRes => {

                if (sparePartsRes["isSucceeded"] === true) {
                    this.spareParts = sparePartsRes["data"];
                    this.showLoading = false;
                }
            });
    }

    searchRepairRequestSparParts() {
        this.showLoading = true;

        const queryStringParams = this.ctrlSearchCriteria.getQueryStringParams();

        this.repairRequestSparePartService.getSparePartReportByQueryString(queryStringParams).subscribe(
            result => {
                this.repairRequestSpareParts = result["data"];
                this.showNoDataFoundLabel = this.repairRequestSpareParts.length === 0;
                this.showLoading = false;
            },
            (error) => {
                console.log(`Error: ${error}`);
            }
        );
    }

    clearSearch() {
        this.repairRequestSpareParts = [];
        this.searchParams.resetValues();
    }

    printRepairRequestSpareParts(): void {

        const queryStringParams = this.ctrlSearchCriteria.getQueryStringParams();

        // SparePartsUsageReport.rdl report.
        this.commonService.print2(5, queryStringParams);
    }
}
