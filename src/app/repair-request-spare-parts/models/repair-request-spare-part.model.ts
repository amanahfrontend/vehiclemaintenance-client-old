﻿export class RepairRequestSparePartSearchParams {
    sparePartId?: number;
    vehicleFleetNumber: string;
    startDate?: string;
    endDate?: string;
    jobCardId?: string;

    constructor() {
        this.resetValues();
    }

    resetValues() {
        this.sparePartId = 0;
        this.vehicleFleetNumber = "";
        this.startDate = "";
        this.endDate = "";
        this.jobCardId = "";
    }
}