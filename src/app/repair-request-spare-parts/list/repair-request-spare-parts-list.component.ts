import { MdlDialogService } from "@angular-mdl/core";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ISparePartsRequest } from "../../shared/Interfaces/ISparePartsRequest.interface";
import { RepairRequestSparePartService } from "../services/repair-request-spare-part.service";

@Component({
    selector: "app-repair-request-spare-parts-list",
    templateUrl: "./repair-request-spare-parts-list.component.html"
})
export class RepairRequestSparePartsListComponent implements OnInit {

    private pRepairRequestId: number = 0;
    requestSpareParts: ISparePartsRequest[];

    @Output() editButtonClicked = new EventEmitter();

    @Input()
    set repairRequestId(value: number) {
        this.pRepairRequestId = value;

        this.getRepairRequestSparParts();
    }

    get repairRequestId(): number {
        return this.pRepairRequestId;
    }

    @Input() pendingSpareParts: boolean = false;

    @Input() showEditButton: boolean = false;
    @Input() showDeleteButton: boolean = false;
    @Input() showTotalCost: boolean = false;

    constructor(
        private repairRequestSparePartService: RepairRequestSparePartService,
        private dialogService: MdlDialogService) { }

    ngOnInit() {
    }

    getRepairRequestSparParts() {

        if (isNaN(this.repairRequestId) || this.repairRequestId <= 0) {
            return;
        }

        let rowStatusId: number = this.pendingSpareParts ? 0 : 1;

        this.repairRequestSparePartService.getByRepairRequest(this.repairRequestId, rowStatusId).subscribe(
            sparePartsRes => {
                if (sparePartsRes && sparePartsRes.isSucceeded === true) {
                    this.requestSpareParts = sparePartsRes["data"];
                }
            });
    }

    editRepairRequestSparePart(repairRequestSparePart: any) {
        this.editButtonClicked.emit(repairRequestSparePart);
    }

    deleteRepairRequestSparePart(repairRequestSparePartId: number) {
        const question = "Are you sure you want to delete this spare part of the repair request?";
        const result = this.dialogService.confirm(question, "No", "Yes");

        // if you need both answers
        result.subscribe(() => {

            this.repairRequestSparePartService.deleteRepairRequestSparePart(repairRequestSparePartId).subscribe(
                responseResult => {

                    if (responseResult["isSucceeded"] === true) {
                        this.getRepairRequestSparParts();
                    } else {
                        this.dialogService.alert("Deleting failed. " + responseResult.message);
                    }
                });
        });
    }
}
