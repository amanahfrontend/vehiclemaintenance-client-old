import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { RepairRequestSparePartService } from "../repair-request-spare-parts/services/repair-request-spare-part.service";
import { ISparePartsRequest } from "../shared/Interfaces/ISparePartsRequest.interface";

@Component({
    selector: "app-nonapprovedrequestsparepartsdetails",
    templateUrl: "./nonapprovedrequestsparepartsdetails.component.html",
    providers: [RepairRequestSparePartService]
})
export class NonApprovedRequestSparePartsDetailsComponent implements OnInit {
    sparepartsrequest: ISparePartsRequest[];
    Id: string = sessionStorage.getItem("id");
    id: number;
    model: any = {};
    router: Router;

    private sub: any;

    constructor(router: Router,
        private dialogService: MdlDialogService,
        private sparepartsrequestservice: RepairRequestSparePartService,
        private route: ActivatedRoute) {
        this.router = router;
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params["id"];
        });

        this.sparepartsrequestservice.getById(this.id).subscribe(
            requestSparePartsRes => {
                if (requestSparePartsRes["isSucceeded"] === true) {
                    this.sparepartsrequest = requestSparePartsRes["data"];
                    this.model = requestSparePartsRes["data"];
                    console.log(this.model);
                } else {
                    console.log(requestSparePartsRes.message);
                }
            });
    }

    delete() {

        this.sparepartsrequestservice.deleteRepairRequestSparePart(this.id).subscribe(
            jobcardRes => {

                if (jobcardRes["isSucceeded"] === true) {
                    this.router.navigateByUrl("/home/prejobcards");
                }
            });
    }

    approve() {

        this.model.rowStatusId = 1;

        this.sparepartsrequestservice.updateRepairRequestSparePart(this.model).subscribe(
            sparepartsrequestRes => {

                if (sparepartsrequestRes["isSucceeded"] === true) {
                    let result = this.dialogService.alert("Approved Successfully");
                    result.subscribe(() => console.log("success"));
                    this.router.navigateByUrl("/home/pending-repair-requests-spare-parts");
                } else {
                    this.dialogService.alert("some thing wrong");
                }
            });
    }
}
