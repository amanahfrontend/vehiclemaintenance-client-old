﻿export class Vehicle {
    id: string;
    plateNumber: string;
    fleetNumber: string;
    chassisNumber: string;
    year: string;
    make: string;
    model: string;
    color: string;
    kiloMeters: string;
    vin: string;
    purchaseDate: Date;
    odometer: number;
    fuelConsumption: number;
    workIdleTime: string;
    type: boolean;
    vehicleStatus: string;
    remarks: string;
}


export class VehiclesSearchParams {

    vehiclePlateNo: string;
    vehicleFleetNo: string;
    model: string;
    vehicleStatus: string;

    constructor() {
        this.resetFields();
    }

    resetFields(): void {
        this.vehiclePlateNo = "";
        this.vehicleFleetNo = "";
        this.model = "";
        this.vehicleStatus = "All";
    }
}