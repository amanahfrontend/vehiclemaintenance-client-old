import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { JobCardAddComponent } from "app/job-cards/add/job-card-add.component";
import { JobCardService } from "app/job-cards/services/job-card.service";
import { Pagination } from "../../shared/components/pagination/pagination.model";
import { CommonService } from "../../shared/services/common.service";
import { DialogService } from "../../shared/services/dialog.service";
import { VehicleAddEditComponent } from "../add-edit/vehicle-add-edit.component";
import { VehiclesSearchParams } from "../models/vehicle.model";
import { VehicleService } from "../services/vehicle.service";

@Component({
    selector: "app-vehicles-list",
    templateUrl: "./vehicles-list.component.html",
    styleUrls: ["./vehicles-list.component.scss"],
    providers: [VehicleService, JobCardService, CommonService]
})
export class VehiclesListComponent implements OnInit {

    // Vehicle Pagination object.
    vehiclePagination: Pagination = {
        pageNo: 1,
        pageSize: 30,
        totalRecords: 0
    };

    vehicles: any = [];
    isActive: boolean = false;
    selectedVehicle: any = {};
    selectedVehicleId: any;
    jobCards: any = [];
    openJobCards: any = [];
    jobCardIdToAddRepairRequestTo: string;
    showModel: boolean = false;
    visible = false;
    visibleAnimate = false;
    showModelRepair: boolean = false;

    vehiclesSearchParams: VehiclesSearchParams;
    showLinkGetAll: boolean = true;

    constructor(
        private vehicleService: VehicleService,
        private customDialogService: DialogService,
        private router: Router,
        private jobCardService: JobCardService,
        private dialog: MatDialog,
        private commonService: CommonService
    ) { }

    ngOnInit() {
        this.vehiclesSearchParams = new VehiclesSearchParams();

        this.searchVehicles();
    }

    pageChanged(event) {
        this.vehiclePagination.pageNo = event.pageNo;
        this.vehiclePagination.pageSize = event.pageSize;

        this.getVehiclesData();
    }

    openJobCardReport() {
        const queryStringParams = `vehicleId=${this.selectedVehicle.id}`;
        this.commonService.print2(4, queryStringParams);
    }

    openAddVehicle(): void {

        const dialogOptions = {
            width: "850px",
            height: "670px",
            data: { id: 0 }
        };

        const dialogRef = this.dialog.open(VehicleAddEditComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.searchVehicles();
        });
    }

    openEditVehicle(vehicleId): void {
        if (!this.selectedVehicleId) {
            this.customDialogService.alert("Note", "Please select a vehicle first", false);
        } else {

            const dialogOptions = {
                width: "850px",
                height: "670px",
                data: { id: vehicleId }
            };

            const dialogRef = this.dialog.open(VehicleAddEditComponent, dialogOptions);

            dialogRef.afterClosed().subscribe(
                () => {
                    this.searchVehicles();
                });
        }
    }

    validateAndOpenAddJobCardDialog(vehicleId: number): void {

        if (!vehicleId) {
            this.customDialogService.alert("Note", "Please, select vehicle first.", false);
        } else {
            this.jobCardService.canAddJobCard(vehicleId).subscribe(
                result => {

                    if (result.data === false) {
                        const message = "The vehicle has open job cards. You can't add any job cards now.";
                        this.customDialogService.alert("Warning", message, false);
                    } else {
                        setTimeout(() => (this.visibleAnimate = true), 100);

                        this.openAddJobCardDialog(vehicleId);
                    }
                },
                () => {
                    console.log("error");
                }
            );
        }
    }

    private openAddJobCardDialog(vehicleId: number) {

        const dialogOptions = {
            width: "750px",
            height: "670px",
            data: { vehicleId: vehicleId, id: 0 }
        };

        const dialogRef = this.dialog.open(JobCardAddComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.searchVehicles();
        });
    }

    getVehiclesData() {
        const vehiclesSearchParams2 = this.getVehicleSearchParamsObject();

        this.vehicleService.searchVehicles(vehiclesSearchParams2,
            this.vehiclePagination.pageNo, this.vehiclePagination.pageSize).subscribe(

                vehicleRes => {
                    if (vehicleRes["isSucceeded"] === true) {
                        this.vehicles = vehicleRes["data"];
                        this.isActive = false;
                    }
                });
    }

    getVehiclesDataRowsCount() {

        const vehiclesSearchParams2 = this.getVehicleSearchParamsObject();

        this.vehicleService.searchVehiclesRowsCount(vehiclesSearchParams2).subscribe(
            vehicleRes => {
                if (vehicleRes["isSucceeded"] === true) {
                    this.vehiclePagination.totalRecords = vehicleRes["data"];
                }
            });
    }

    private getVehicleSearchParamsObject(): VehiclesSearchParams {
        // Create a new object of VehiclesSearchParams 
        // and replace the word "All" with empty for the property "vehicleStatus".
        const vehiclesSearchParams2 = new VehiclesSearchParams();

        Object.assign(vehiclesSearchParams2, this.vehiclesSearchParams);

        const vehicleStatus = this.vehiclesSearchParams.vehicleStatus === "All"
            ? ""
            : this.vehiclesSearchParams.vehicleStatus;

        vehiclesSearchParams2.vehicleStatus = vehicleStatus;

        return vehiclesSearchParams2;
    }

    searchVehicles() {

        this.isActive = true;

        this.vehiclePagination.pageNo = 1;
        this.getVehiclesDataRowsCount();

        this.getVehiclesData();
    }

    clearSearch() {
        this.vehiclesSearchParams.resetFields();

        this.searchVehicles();
    }

    deleteVehicle(id) {
        if (!this.selectedVehicleId) {

            this.customDialogService.alert("Fail", "Please, select a vehicle first", false);

        } else {
            const question = "Do you really want to delete this vehicle?";
            const result = this.customDialogService.confirm("Confirm Delete", question, true);

            result.subscribe(
                res => {
                    if (res) {
                        this.vehicleService.deleteVehicle(id).subscribe(
                            vehicleRes => {

                                if (vehicleRes["isSucceeded"] === true) {

                                    this.customDialogService.alert("Success", "the vehicle deleted successfully.", false);

                                    this.searchVehicles();

                                } else {
                                    this.customDialogService.alert("Fail", "You can not delete this vehicle.", false);
                                }
                            });
                    }
                },
                () => {
                    this.customDialogService.alert("Fail", "Something wrong", false);
                }
            );
        }
    }

    checkVehicle(vehicleObj) {
        this.isActive = true;

        if (vehicleObj) {
            this.selectedVehicle = vehicleObj;
            this.selectedVehicleId = vehicleObj.id;
            this.isActive = false;
        } else {
            this.selectedVehicleId = "";
            this.isActive = false;
        }
    }

    checkCanAddJobCard(vehicleId) {
        this.jobCardService.canAddJobCard(vehicleId).subscribe(
            result => {
                if (result.data === false) {
                    this.customDialogService.alert("Note", result.status.message, false);
                }
            });
    }

    viewVehicle(id) {
        this.router.navigate(["home/vehicles/details/" + id]);
    }

    getOpenJobCards(vehicleId) {
        this.isActive = true;

        this.jobCardService.getJobCardsOpenByVehicleId(vehicleId).subscribe(
            result => {
                this.openJobCards = result["data"];
                this.isActive = false;
            },
            () => {
                console.log("error");
                this.isActive = false;
            }
        );
    }

    //getJobCards(vehicleId) {
    //    this.jobCardService.getJobCardsByVehicleId(vehicleId).subscribe(
    //        result => {
    //            this.jobCards = result["data"];
    //        },
    //        () => {
    //            console.log("error");
    //        }
    //    );
    //}

    public show(): void {
        if (!this.selectedVehicleId) {
            this.customDialogService.alert(
                "Fail",
                "please, Select vehicle first",
                false
            );
        } else {
            this.isActive = true;
            this.visible = true;

            //this.selectedVehicleId = true;

            //$("#modalJobCard").shows;

            setTimeout(() => {
                this.isActive = false;
                this.visibleAnimate = true;
            },
                100);
        }
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => (this.visible = false), 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    addJobCard() {
        if (!this.selectedVehicleId) {
            this.customDialogService.alert("Fail", "please,pick vehicle first", false);
            this.showModelRepair = false;
            // this.hide()
        } else {
            this.showModelRepair = true;
            //$("#modalJobCard").modal("show");
        }
    }

    showAddRepairRequestDialog() {
        if (!this.selectedVehicleId) {

            this.customDialogService.alert("Fail", "Please select vehicle first.", false);
        } else {
            this.isActive = true;
            if (
                this.openJobCards === [] ||
                this.openJobCards === "" ||
                this.openJobCards == null
            ) {
                this.isActive = true;

                this.customDialogService.alert("Fail", "you should add job card for this vehicle first", false);

                this.showModel = false;
                this.isActive = false;
            } else {
                this.showModel = true;
                this.show();
                this.isActive = false;
            }
        }
    }

    next() {
        this.hide();
        this.showModel = false;
        this.showModelRepair = true;
    }

    getDate(dateString: any): Date {

        const date = new Date(dateString);

        const year = date.getFullYear();

        if (year === 1) {
            return null;
        }

        return date;
    }

    currentUserIsManager() {
        return this.commonService.currentUserIsManager;
    }
}
