import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { IType } from "../shared/Interfaces/IType.interface";
import { IUploadFile } from "../shared/Interfaces/IUploadFile.interface";
import { fileService } from "../shared/services/file.service";
import { IVendor } from "../vendors/models/IVendor.interface";
import { VendorsService } from "../vendors/services/vendors.service";

@Component({
    selector: "app-add-vendor-to-repair-request",
    templateUrl: "./add-vendor-to-repair-request.component.html",
    providers: [fileService, VendorsService]
})
export class AddVendorToRepairRequestComponent implements OnInit {
    router: Router;
    public visible = false;
    public visibleAnimate = false;
    model: any = {};
    private filecontent: string;
    file: any;
    uploadFiles: any;

    tempfiles: {
        'File': IUploadFile,
        'FileName': string,
        'Fk_RepairRequest_ID': number,
        "FK_Vendor_ID": number
    }[] = [];

    tempfile: any = {};
    files: IType[] = [];
    repairrequestid: number;
    vendors: IVendor[];
    fK_Vendor_ID: any = 0;

    constructor(
        private vendorservice: VendorsService,
        private dialogService: MdlDialogService,
        private fileService: fileService) {
    }

    ngOnInit() {
        // get Vendor
        this.vendorservice.getVendors().subscribe(
            vendorRes => {
                if (vendorRes["isSucceeded"] === true) {
                    this.vendors = vendorRes["data"];
                }
            });
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    public shows(files, id): void {
        this.repairrequestid = id;
        console.log(id);
        console.log(this.files);
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    saveAttachments(id) {
        for (let index = 0; index < this.uploadFiles.length; index++) {
            this.tempfiles.push({
                'File': this.uploadFiles[index],
                'FK_Vendor_ID': this.fK_Vendor_ID,
                "FileName": this.uploadFiles[index].name,
                'Fk_RepairRequest_ID': this.repairrequestid
            });
        }

        this.model = this.tempfiles;

        this.fileService.addFile(this.model).subscribe(fileRes => {
            if (fileRes["isSucceeded"] === true) {
                this.visibleAnimate = false;
                setTimeout(() => this.visible = false, 1000);

                //let result = this.dialogService.alert("updated Successfully");
                //result.subscribe(() => {
                this.vendorservice.getVendors().subscribe(vendorRes => {
                    if (vendorRes["isSucceeded"] === true) {
                        this.vendors = vendorRes["data"];
                    }
                    //});
                });

            } else {
                this.dialogService.alert("some thing wrong");
            }
        });
    }

    handleFileSelect(evt) {
        this.uploadFiles = evt.target.files;
        console.log(this.uploadFiles);

        for (let index = 0; index < this.uploadFiles.length; index++) {
            this.file = this.uploadFiles[index];
            this.uploadFiles[index].FileName = evt.target.files[index].name;
            console.log(this.file);

            if (this.uploadFiles && this.file) {
                var reader = new FileReader();
                reader.onload = this._handleReaderLoaded.bind(this.file);
                reader.readAsBinaryString(this.file);
            }
        }
    }

    _handleReaderLoaded(readerEvt) {
        var binaryString = readerEvt.target.result;
        this.filecontent = btoa(binaryString);
        readerEvt.filecontent = this.filecontent;
    }

    getfK_Vendor_ID(evt) {
        console.log(evt.srcElement.selectedIndex);
        this.fK_Vendor_ID = evt.srcElement.selectedIndex;
    }
}

