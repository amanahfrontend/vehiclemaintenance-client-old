import { Component, OnInit } from "@angular/core";
import { User } from "../models/user.model";
import { RoleService } from "../../roles/services/role.service";
import { UserService } from "../services/user.service";

@Component({
    selector: "app-user-profile-edit",
    templateUrl: "./user-profile-edit.component.html",
    providers: [UserService, RoleService]
})
export class UserProfileEditComponent implements OnInit {
    model: User;
    userRole: string = "";

    constructor(private userService: UserService) {
    }

    ngOnInit() {
        this.model = {};
        this.getUserById();
    }

    getUserById() {
        const userId: string = sessionStorage.getItem("id");

        this.userService.getUserById(userId).subscribe(
            userRes => {

                if (userRes && userRes["isSucceeded"] === true) {
                    this.model = userRes["data"];
                    this.userRole = this.model.roleNames[0];
                }
            });
    }
}