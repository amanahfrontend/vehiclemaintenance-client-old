import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DialogService } from "../../shared/services/dialog.service";
import { UserAddComponent } from "../add/user-add.component";
import { UserService } from "../services/user.service";

@Component({
    selector: "app-users-list",
    templateUrl: "./users-list.component.html",
    styleUrls: ["./users-list.component.css"],
    providers: [UserService]
})
export class UsersListComponent implements OnInit {
    users: any = [];
    isActive: boolean = false;
    keyword: string = "";

    constructor(private userService: UserService,
        private customDialogService: DialogService,
        public dialog: MatDialog) {
    }

    ngOnInit() {
        this.searchUsers();
    }
    
    searchUsers() {
        this.userService.searchUsers(this.keyword).subscribe(
            userRes => {
                if (userRes["isSucceeded"] === true) {
                    this.users = userRes["data"];
                    this.isActive = false;
                }
            });
    }

    openAddUser(): void {

        const dlgConfigs = {
            width: "600px",
            data: { id: 0 }
        };

        let dialogRef = this.dialog.open(UserAddComponent, dlgConfigs);

        dialogRef.afterClosed().subscribe(
            () => {
                this.searchUsers();
            });
    }

    deleteUser(username) {
        const question = "Do you really want to delete this user?";
        let result = this.customDialogService.confirm("Confirm", question, true);

        result.subscribe(
            (res) => {
                if (res) {
                    this.userService.deleteUser(username).subscribe(
                        userRes => {

                            if (userRes["isSucceeded"] === true) {

                                this.searchUsers();
                            }
                        });
                }
            },
            () => {
                console.log("Declined");
            }
        );
    }
}
