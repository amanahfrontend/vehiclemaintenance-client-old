﻿import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../services/user.service";

@Component({
    selector: "app-user-change-password",
    templateUrl: "./user-change-password.component.html"
})
export class UserChangePasswordComponent implements OnInit {
    public visible = false;
    public visibleAnimate = false;
    fGroup: FormGroup;

    model: any = {};

    constructor(
        private userService: UserService,
        private formBuilder: FormBuilder) {

        this.fGroup = this.formBuilder.group({
            userName: ["", Validators.required],
            password: ["", Validators.pattern]
        });
    }

    ngOnInit() {

    }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    show(model): void {
        Object.assign(this.model, model);

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    updateUserPassword() {
        
        this.userService.updateUserPassword(this.model).subscribe(
            userRes => {

                if (userRes["isSucceeded"] === true) {
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);
                }
            });
    }
}