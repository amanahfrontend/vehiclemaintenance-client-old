import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./login.component";

const loginRoutes: Routes = [
    { path: "login", component: LoginComponent }
];

@NgModule({
    imports: [
        //CommonModule,
        //RouterModule.forChild(loginRoutes)
    ],
    declarations: [
        //LoginComponent,
        //RouterModule
    ],
    //exports: [RouterModule]
})
export class LoginModule { }