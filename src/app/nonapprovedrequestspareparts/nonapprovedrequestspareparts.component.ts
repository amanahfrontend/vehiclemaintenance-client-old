import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { RepairRequestSparePartService } from "../repair-request-spare-parts/services/repair-request-spare-part.service";

@Component({
    selector: "app-nonapprovedrequestspareparts",
    templateUrl: "./nonapprovedrequestspareparts.component.html",
    styleUrls: ["./nonapprovedSpare.component.css"],
    providers: [RepairRequestSparePartService]
})
export class NonApprovedRequestSparePartsComponent implements OnInit {
    repairRequestSpareParts: any = [];
    type: boolean;
    isActive: boolean = false;

    constructor(
        private repairRequestSparePartService: RepairRequestSparePartService,
        private dialogService: MdlDialogService
    ) {
    }

    ngOnInit() {
        this.getRepairRequestSpareParts();
    }

    getRepairRequestSpareParts() {
        this.isActive = true;

        this.repairRequestSparePartService.getPendingRepairRequestSpareParts().subscribe(
            repairRequestSparePartsRes => {

                if (repairRequestSparePartsRes["isSucceeded"]) {
                    this.repairRequestSpareParts = repairRequestSparePartsRes["data"];
                }

                this.isActive = false;
            });
    }

    approve(model) {
        const question = "Are you sure you want to approve this spare part?";
        const result = this.dialogService.confirm(question, "Later", "Yes");

        // if you need both answers
        result.subscribe(
            () => {
                model.rowStatusId = 1;

                this.repairRequestSparePartService.updateRepairRequestSparePart(model).subscribe(
                    sparepartsrequestRes => {
                        if (sparepartsrequestRes["isSucceeded"] === true) {
                            //const result = this.dialogService.alert("updated Successfully");
                            //result.subscribe(() => this.getAllSpares());
                            this.getRepairRequestSpareParts();
                        } else {
                            this.dialogService.alert("some thing wrong");
                        }
                    });
            },
            () => {
                console.log("declined");
            }
        );
    }
}