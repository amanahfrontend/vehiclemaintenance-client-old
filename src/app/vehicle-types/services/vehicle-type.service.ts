import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { WebService } from "../../shared/services/webService";

@Injectable()
export class VehicleTypeService {
    constructor(private http: HttpClient, private config: WebService) { }

    public getVehicleTypes(): Observable<any> {
        const url = this.config.baseUrl + "VehicleType/Get";

        return this.http.get(url).map(data => {
            const responseData = <any>data;
            return responseData;
        });
    }

    public getVehicleType(id: number): Observable<any> {
        const url = this.config.baseUrl + "VehicleType/Get/" + id;

        return this.http.get(url).map(data => {
            const result = <any>data;
            return result;
        });
    }

    public searchVehicleTypes(name: string): any {
        const urlStatic = "VehicleType/SearchByName?word={0}";
        const url = this.config.baseUrl + urlStatic.replace("{0}", name);

        return this.http.get(url).map(data => {
            const result = <any[]>data;
            return result;
        });
    }

    public addVehicleType(model: any): any {
        model.rowStatusId = 1;
        model.createdByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "VehicleType/Add";

        return this.http.post(url, model).map(
            data => {
                const response = <any>data;
                return response;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    public updateVehicleType(model): Observable<any> {
        model.rowStatusId = 1;
        model.modifiedByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "VehicleType/Update";

        return this.http.put(url, model).map(
            data => {
                const response = <any>data;
                return response;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    public deleteVehicleType(id: number): any {
        let url = "{0}VehicleType/Delete/{1}/{2}";

        url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
            .replace("{2}", this.config.currentUserId);

        return this.http.delete(url).map(
            data => {
                const response = <any>data;
                return response;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }
}
