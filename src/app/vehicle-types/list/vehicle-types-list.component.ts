import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { DialogService } from "../../shared/services/dialog.service";
import { VehicleTypeAddEditComponent } from "../add-edit/vehicle-type-add-edit.component";
import { VehicleTypeService } from "../services/vehicle-type.service";

@Component({
    selector: "app-vehicle-types-list",
    templateUrl: "./vehicle-types-list.component.html",
    styleUrls: ["./vehicle-types-list.component.css"],
    providers: [VehicleTypeService]
})
export class VehicleTypesListComponent implements OnInit {
    vehicleTypes: any = [];
    isActive: boolean = false;
    query: string = "";

    constructor(
        private vehicleTypeService: VehicleTypeService,
        private customDialogService: DialogService,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
        this.isActive = true;
        this.getVehicleTypes();
    }

    getVehicleTypes() {
        this.vehicleTypeService.getVehicleTypes().subscribe(
            vehicleTypeRes => {
                if (vehicleTypeRes["isSucceeded"] === true) {
                    this.vehicleTypes = vehicleTypeRes["data"];
                    this.isActive = false;
                }
            });
    }

    openAddEditDialog(id: number): void {
        const dialogOptions = {
            width: "650px",
            height: "300px",
            data: { id: id }
        };

        const dialogRef = this.dialog.open(VehicleTypeAddEditComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.getVehicleTypes();
        });
    }

    private getVehicleTypesBySearch() {
        this.vehicleTypeService.searchVehicleTypes(this.query).subscribe(
            vehicleTypeRes => {
                if (vehicleTypeRes["isSucceeded"] === true) {
                    this.vehicleTypes = vehicleTypeRes["data"];
                    this.isActive = false;
                }
            });
    }

    searchVehicleTypes() {
        if (this.query) {
            this.getVehicleTypesBySearch();
        } else {
            this.getVehicleTypes();
        }
    }

    deleteVehicleType(id) {

        const question = "Do you really want to delete this record?";
        const result = this.customDialogService.confirm("Confirm Delete", question, true);

        result.subscribe(
            res => {
                if (res) {
                    this.vehicleTypeService.deleteVehicleType(id).subscribe(
                        vehicleType1Res => {
                            if (vehicleType1Res["isSucceeded"] === true) {

                                this.getVehicleTypes();
                            }
                        });
                }
            },
            () => {
                console.log("declined");
            }
        );
    }
}
