import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleService } from 'app/vehicles/services/vehicle.service';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { ISparePartStatus } from '../shared/Interfaces/ISparePartStatus.interface';
import { CommonVariables } from '../shared/services/common-variables.service';
import { DialogService } from '../shared/services/dialog.service';
import { SparePartStatusService } from '../shared/services/sparepartstatus.service';
import { UsedMaterialsService } from '../shared/services/usedmaterials.service';
import { SparePart } from "../spare-parts/models/spare-part.model";
import { SparePartService } from "../spare-parts/services/spare-part.service";
import { Technician } from '../technicians/models/technician.model';
import { TechnicianService } from '../technicians/services/technician.service';
import { Vehicle } from "../vehicles/models/vehicle.model";

@Component({
    selector: 'app-addusedMaterials',
    templateUrl: './addusedMaterials.component.html',
    styleUrls: ['./addusedMaterials.component.css'],
    providers: [TechnicianService, UsedMaterialsService, VehicleService, SparePartService, SparePartStatusService, CommonVariables]
})

export class AddUsedMaterialsComponent implements OnInit {
    router: Router;
    public visible = false;
    public visibleAnimate = false;
    model: any = {};
    technicians: Technician[];
    spareparts: SparePart[];
    sparepartstatus: ISparePartStatus[];
    transferFrom: string = '';
    type: boolean;
    name: string;
    nameList: string[];
    showDropDown: boolean;
    statusvalue: number;
    vehicles: Vehicle[];

    public Options: Observable<any[]>;
    searchTerms = new Subject<string>();
    public flag: boolean = true;

    constructor(_router: Router, private customDialogService: DialogService,
        private usedMaterialsservice: UsedMaterialsService, private commonS: CommonVariables,
        private technicianservice: TechnicianService,
        private sparePartService: SparePartService,
        private vehicleservice: VehicleService,
        private sparepartstatusservice: SparePartStatusService) {
        this.router = _router;
    }

    ngOnInit() {
        this.showDropDown = false;
        ////////// get Tchnicians ///////////////
        this.technicianservice.getTechnicians().subscribe(technicianRes => {
            if (technicianRes["isSucceeded"] == true) {
                this.technicians = technicianRes["data"];
            }
            return this.technicians;
        });

        // get spare parts
        this.sparePartService.getSpareParts().subscribe(
            sparePartsRes => {
                if (sparePartsRes["isSucceeded"] === true) {
                    this.spareparts = sparePartsRes["data"];
                }

            });
        ////////// get Spare Part Status ///////////////
        this.sparepartstatusservice.getSparePartStatus().subscribe(sparepartstatusRes => {
            if (sparepartstatusRes["isSucceeded"] == true) {
                this.sparepartstatus = sparepartstatusRes["data"];
            }
            return this.sparepartstatus;
        });

        //// get Vehicles
        //this.vehicleservice.getVehicles().subscribe(vehicleRes => {
        //    if (vehicleRes["isSucceeded"] === true) {
        //        this.vehicles = vehicleRes["data"];
        //    }
        //});

        //$(function () {
        //    $("#fK_SparePartStatus_ID").change(function () {
        //        this.statusvalue = $('#fK_SparePartStatus_ID').val();
        //        if (this.statusvalue == "3: 4") {
        //            $("#ForfleetNumber").removeClass("hide");
        //        }
        //        else {
        //            if (!$("#ForfleetNumber").hasClass("hide")) {
        //                $("#ForfleetNumber").addClass("hide");
        //                $("#fleetNumber").val("");
        //            }
        //        }
        //    });
        //});
    }

    public show(): void {
        this.model = {};
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }
    public hide(): void {
        this.model = {};
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }
    public showjobcardid(fk_jobcard): void {
        this.model = {};
        this.model.FK_JobCard_ID = fk_jobcard;
        console.log(this.model.FK_JobCard_ID);
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }
    public showrepairrequestid(fk_repairrequest): void {
        this.model = {};
        this.model.FK_RepairRequest_ID = fk_repairrequest;
        this.commonS.repairId = fk_repairrequest;

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

    saveUsedMaterial() {
        console.log(JSON.stringify(this.model))
        this.model.FK_TranferFromVehicle_Id = this.transferFrom;
        this.usedMaterialsservice.addRepairRequestMaterial(this.model).subscribe(usedMaterialsRes => {
            if (usedMaterialsRes["isSucceeded"] == true) {
                this.visibleAnimate = false;
                setTimeout(() => this.visible = false, 300);
                this.customDialogService.alert('Done', 'added successfully', false);
                console.log(JSON.stringify(this.model))
                location.reload();
            }
            else {
                this.customDialogService.alert('Fail', 'some thing wrong', false);
            }
        });
    }

    search() {
        this.showDropDown = !this.showDropDown;
        console.log(this.name);
        this.sparePartService.searchInventory(this.name).subscribe(inventoryRes => {
            if (inventoryRes["isSucceeded"] == true) {
                this.spareparts = inventoryRes["data"];

            }
            return this.spareparts;
        });
    }
}
