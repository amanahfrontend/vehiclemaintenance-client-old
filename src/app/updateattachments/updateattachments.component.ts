import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit, ViewChild, EventEmitter, Output } from "@angular/core";
import { VendorsService } from "app/vendors/services/vendors.service";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { IVendor } from "../vendors/models/IVendor.interface";
import { FileUploaderComponent } from "../file-uploader/file-uploader.component";
import { RepairRequestVendor } from "../repair-requests/models/repair-request-vendor.model";
import { RepairRequestVendorService } from "app/repair-requests/services/repair-request-vendor.service";
import { FileUploadService } from "../shared/services/file-upload.service";

@Component({
    selector: "app-updateattachments",
    templateUrl: "./Updateattachments.component.html",
    styleUrls: ["./updateattachments.component.css"],
    providers: [VendorsService, RepairRequestVendorService, FileUploadService]
})
export class UpdateattachmentsComponent implements OnInit {

    @ViewChild("fileUploader") fileUploader: FileUploaderComponent;
    @Output() dataChanged = new EventEmitter();

    repairRequestVendors: RepairRequestVendor[] = [];
    vendors: IVendor[];

    files: any = [];

    visible = false;
    visibleAnimate = false;

    model: any = {};
    repairRequestId: number;
    vendorId: number = 0;

    constructor(
        private vendorService: VendorsService,
        private repairRequestVendorService: RepairRequestVendorService,
        private fileUploadService: FileUploadService,
        private dialogService: MdlDialogService) {

    }

    ngOnInit() {

        this.getVendors();
    }

    getRepairRequestVendors() {

        this.repairRequestVendorService.getByRepairRequest(this.repairRequestId).subscribe(
            responseResult => {
                if (responseResult["isSucceeded"] === true) {
                    this.repairRequestVendors = responseResult.data;
                }
            }
        );
    }

    getVendors() {

        this.vendorService.getVendors().subscribe(
            vendorResponse => {
                if (vendorResponse["isSucceeded"] === true) {
                    this.vendors = vendorResponse["data"];
                }
            });
    }

    hide(): void {
        this.visibleAnimate = false;

        setTimeout(() => {
            this.getVendors();
            this.visible = false;
        }, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    showDialog(files, id): void {
        this.files = files;

        this.repairRequestId = id;
        this.getRepairRequestVendors();

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    saveAttachments() {

        if (this.fileUploader.uploadedFiles.length === 0) {
            this.dialogService.alert("Please select files.");
            return;
        }

        const model: RepairRequestVendor = {
            id: 0,
            repairRequestId: this.repairRequestId,
            vendorId: this.vendorId,
            files: this.fileUploader.uploadedFiles,
            rowStatusId: 1,
            createdByUserId: null,
            modifiedByUserId: null
        };

        this.repairRequestVendorService.addRepairRequestVendor(model).subscribe(
            jobCardRes => {

                // Upload files after saving the data.
                this.uploadFilesAfterSavingData(jobCardRes);
            });
    }

    private uploadFilesAfterSavingData(responseResult) {
        if (responseResult["isSucceeded"] === true) {

            const filesCount: number = responseResult.data.fileList.filePaths.length;

            if (filesCount === 0) {
                return;
            }

            const filePaths: string[] = [];

            for (let i = 0; i < filesCount; i++) {

                filePaths.push(responseResult.data.fileList.filePaths[i]);
            }

            this.fileUploader.uploadFiles(filePaths);
        }
    }

    uploadingFilesComplete() {
        this.getRepairRequestVendors();
        this.dataChanged.emit();
    }

    deleteFile(id: number) {
        const result = this.dialogService.confirm("Are you sure, delete this file?", "No", "Yes");

        // if you need both answers
        result.subscribe(
            () => {

                this.fileUploadService.deleteFile(id).subscribe(
                    responseResult => {
                        if (responseResult["isSucceeded"] === true) {
                            this.getRepairRequestVendors();
                            this.dataChanged.emit();
                        }
                    });
            },
            () => {
                //console.log("declined");
            }
        );
    }

    deleteRepairRequestVendor(id: number) {
        const result = this.dialogService.confirm("Are you sure, delete this vendor?", "No", "Yes");

        // if you need both answers
        result.subscribe(
            () => {

                this.repairRequestVendorService.deleteRepairRequestVendor(id).subscribe(
                    responseResult => {
                        if (responseResult["isSucceeded"] === true) {
                            this.getRepairRequestVendors();
                            this.dataChanged.emit();
                        }
                    });
            },
            () => {
                //console.log("declined");
            }
        );
    }
}
