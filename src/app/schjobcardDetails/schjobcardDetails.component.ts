import { MdlDialogService } from "@angular-mdl/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { JobCard } from "app/job-cards/models/job-card.model";
import { JobCardService } from "app/job-cards/services/job-card.service";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { RepairRequestService } from "../repair-requests/services/repair-request.service";
import { IJobCard_Technician } from "../shared/Interfaces/IJobCard_Technician.interface";
import { IRepairRequest } from "../shared/Interfaces/IRepairRequest.interface";
import { IUsedMaterials } from "../shared/Interfaces/IUsedMaterials.interface";
import { DialogService } from "../shared/services/dialog.service";
import { JobCard_TechnicianService } from "../shared/services/jobcard_technician.service";
import { UsedMaterialsService } from "../shared/services/usedmaterials.service";



@Component({
    selector: "app-SchjobcardDetails",
    templateUrl: "./SchjobcardDetails.component.html",
    styleUrls: ["./SchjobcardDetails.component.css"],
    providers: [JobCardService, RepairRequestService, JobCard_TechnicianService, UsedMaterialsService]
})
export class SchJobCardDetailsComponent implements OnInit {
    jobCard: JobCard[];
    usedMaterials: IUsedMaterials[];
    jobCardTechnicians: IJobCard_Technician[];
    Id: string = sessionStorage.getItem("id");
    id: number;
    model: any = {};
    router: Router;
    fk_jobcard: number;
    showaddRepairRequest: boolean;
    private sub: any;
    isActive: boolean = false;
    repairrequests: IRepairRequest[];

    constructor(_router: Router,
        private customDialogService: DialogService,
        private dialogService: MdlDialogService,
        private repairRequestservice: RepairRequestService,
        private jobcardservice: JobCardService,
        private usedmaterialsservice: UsedMaterialsService,
        private jobcard_technicianservice: JobCard_TechnicianService,
        private route: ActivatedRoute) {
        this.router = _router;
    }

    ngOnInit() {
        this.isActive = true;
        this.sub = this.route.params.subscribe(params => {
            this.id = +params["id"];
            this.fk_jobcard = this.id;
        });
        this.jobcardservice.getJobCardById(this.id).subscribe(jobCardRes => {
            if (jobCardRes["isSucceeded"] == true) {
                this.jobCard = jobCardRes["data"];
                this.model = jobCardRes["data"];

                this.isActive = false;
            }
            return this.jobCard;
        });
        this.repairRequestservice.getByJobCardId(this.fk_jobcard).subscribe(repairrequestRes => {
            this.isActive = true;
            if (repairrequestRes["isSucceeded"] == true) {
                this.repairrequests = repairrequestRes["data"];
                this.isActive = false;
            }
            return this.repairrequests;
        });
        this.usedmaterialsservice.getByjobcardId(this.fk_jobcard).subscribe(usedmaterialRes => {
            this.isActive = true;
            if (usedmaterialRes["isSucceeded"] == true) {
                this.usedMaterials = usedmaterialRes["data"];
                this.isActive = false;
            }
            return this.usedMaterials;
        });
        this.jobcard_technicianservice.getByjobcardId(this.fk_jobcard).subscribe(jobcard_technicianRes => {
            this.isActive = true;
            if (jobcard_technicianRes["isSucceeded"] == true) {
                this.jobCardTechnicians = jobcard_technicianRes["data"];
                this.isActive = false;
            }
            return this.jobCardTechnicians;
        });
    }

    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById("print-section").innerHTML;
        popupWin = window.open("", "_blank", "top=0,left=0,height=100%,width=auto");
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Job Card Details </title>
              <style>
              //........Customized style.......
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}
        <br>
        <br>
        <label style="font:bolder; font-style: normal;font-size: 20px; color:black">  Signature </label>
        </body>
          </html>`
        );
        popupWin.document.close();
    }

    delete() {
        let result = this.dialogService.confirm("Are you sure, you want to delete this?", "No", "Yes");
        // if you need both answers
        result.subscribe(() => {

            this.jobcardservice.deleteJobCard(this.id).subscribe(jobcardRes => {
                console.log(jobcardRes);
                if (jobcardRes["isSucceeded"] == true) {
                    this.router.navigateByUrl("/home/jobcards");

                }
            })
        })
    }
    deleteRepairrequest(id) {
        let result = this.dialogService.confirm("Are you sure, you want to delete this?", "No", "Yes");
        // if you need both answers
        result.subscribe(() => {

            this.repairRequestservice.deleteRepairRequest(id).subscribe(repairrequestRes => {
                
                if (repairrequestRes["isSucceeded"] == true) {
                    this.router.navigateByUrl("/home/schrepairrequests");
                    this.customDialogService.alert("Done", "added successfully", false);
                    location.reload();
                }
                else {
                    this.customDialogService.alert("Fail", "some thing wrong", false);
                }
            });
        })
    }
}


