import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleService } from 'app/vehicles/services/vehicle.service';
import { equipmentTransferService } from '../../shared/services/equipmentTransferReport.service';


@Component({
  selector: 'app-equipmentReport',
  templateUrl: './equipment.component.html',
  styleUrls:['./equipment.component.css'],
  providers: [equipmentTransferService, VehicleService]
})

export class equipmentReportComponent implements OnInit {


  constructor(_router: Router, private equipmentTransferS: equipmentTransferService, private vehicleservice: VehicleService) {
  }

  isActive: boolean = false;
  equipments: any = [];
  startDate: Date;
  dateReport: any;
  fleetNum: string;
  fleetNumTo:string;
  vehicles: any= [];


  ngOnInit() {
    this.getAllequipmentTransfer();
    this.getAllFleetNum();
  }

  stringDate(date) {
    let d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(),
      date.getMinutes(),
      date.getSeconds()));
    return d.toISOString().split('T')[0]
  }

  getAllequipmentTransfer() {
    this.equipmentTransferS.getAllEquipments().subscribe(result => {
      this.equipments = result['data'];
      console.log(JSON.stringify(this.equipments))
    }, error => {
      console.log('error')
    })
  }

  getAllequipmentTransferByDate() {
    if (this.startDate) {
      this.dateReport = this.stringDate(this.startDate);
      console.log(this.dateReport)
    }

    this.equipmentTransferS.getAllEquipmentsByDate(this.dateReport).subscribe(result => {
      this.equipments = result['data'];;
      console.log(JSON.stringify(this.equipments))
    }, error => {
      console.log('error')
    })
  }

  clearEndDate() {
    this.startDate = null;
    this.getAllequipmentTransfer();
    this.equipments = [];
  }
  
  clearSheaarch(){
    this.startDate = null;
    this.fleetNum = '';
    this.fleetNumTo = '';
    this.getAllequipmentTransfer();
    this.equipments = [];
  }

  getAllFleetNum(){
    this.vehicleservice.getVehicles().subscribe(vehicleRes => {
      if (vehicleRes["isSucceeded"] == true) {
        this.vehicles = vehicleRes["data"];
      }
      }, error => {
        console.log('error')
      })
  }

  getAllEquipmentWithFleetNumFrom(fleetNumber) {
    this.fleetNum = fleetNumber;
    // this.vehicleservice.getVehicles().subscribe(vehicleRes => {
    //   if (vehicleRes["isSucceeded"] == true) {
    //     this.vehicles = vehicleRes["data"];
    //     for (let i = 0; i < this.vehicles.length; i++) {
    //       this.fleetNum = this.vehicles[i].fleetNumber;

    //       if (this.vehicles[i].fleetNumber !== this.fleetNum) {
    //         this.dialogService.alert('fleet number you enter not exist, please select another one')
    //       }
    //       else{
            this.equipmentTransferS.getAllEquipmentsByFleetFrom(this.fleetNum).subscribe(result =>{
              this.equipments = result['data'];
              console.log(JSON.stringify(this.equipments))
            },error =>{
              console.log('error')
            })
          // }
        // }
    //   }
    // }, error => {
    //   console.log('error')
    // })
  }
  getAllEquipmentWithFleetNumTo(fleetNumber) {
    this.fleetNumTo = fleetNumber;
            this.equipmentTransferS.getAllEquipmentsByFleetTo(this.fleetNumTo).subscribe(result =>{
              this.equipments = result['data'];
              console.log(JSON.stringify(this.equipments))
            },error =>{
              console.log('error')
            })

  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          
          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap1.min.css"/>
          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
          <style>
          //........Customized style.......
         
          label {
            display: inline-block;
            margin-bottom: .5rem;
          }
          .col-md-12 {
            width: 100%;
            float: left;
          }
        
          .form-group {
            margin-bottom: 1rem;
          }
          .text-center {
            text-align: center!important;
          }
          .col-md-6 {
            width: 50%;
          }
          .col-md-offset-3 {
            margin-left: 25%;
          }
          .pull-right {
            float: right!important;
          }
          .pull-left {
            float: left!important;
          }
          .col-md-2 {
            width: 16.66666667%;
          }
          .col-md-4 {
            width: 33.33333333%;
          }
          div {
            display: block;
          }
       
        .alert-info{
          background: #555 !important;
        }
        .alert-info span{
          color:#fff !important;
        }
        .table-bordered {
            border: 1px solid #a3a4a5 !important;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #797979 !important;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
          border-top: none;
      }
        tr th{
            font-weight: 600 !important;
        }
        h5{
            font-weight: 600 !important;  
        }
        .table-bordered>thead>tr>th{
          background: #555 !important;
          color:#fff !important;
        }
        hr{
          border-top: 1px solid #000 !important;
          margin-top:-20px !important;
        }
        tr td hr{
          margin-top:10px !important;
        }
        input{
          color:#000 !important;
        }
        .reportTitle{
          margin-left:-38px;
          font-weight: 700;
        }
        .alert-info{
          margin-top: 2%;
      }
      .approveSign{
        height: 80px;
      }
      .dateHr{
        margin-left:150px !important;
        margin-top:-20px !important;
      }
      ins{
        margin-left:20px !important;
      }
      .form-control
      {
        border: 0;
        padding:0;
        overflow:visible;
      }
      input::-webkit-input-placeholder{
        display:inline;
      }
     .reportTitle{
       margin-left:1px !important;
     }
        * {
          -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
          color-adjust: exact !important;                 /*Firefox*/
        }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }


}