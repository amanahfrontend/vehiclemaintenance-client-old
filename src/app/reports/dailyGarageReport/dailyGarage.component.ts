//// This component is not used.
//import { Component, OnInit } from '@angular/core';
//import { ActivatedRoute, Router } from '@angular/router';
//import { garage_receiptReportService } from '../../shared/services/garage_receiptReport.service';
//import { jobCardService } from '../../shared/services/jobcard.service';
//import { vehicleService } from '../../shared/services/vehicle.service';

//@Component({
//    selector: 'app-dailyGarageReport',
//    templateUrl: './dailyGarage.component.html',
//    styleUrls: ['./dailyGarage.component.css'],
//    providers: [jobCardService, vehicleService, garage_receiptReportService]
//})
//export class dailyGarageReportComponent implements OnInit {


//    constructor(_router: Router, private vehicleservice: vehicleService, private jobCardS: jobCardService,

//        private garage_receiptReportS: garage_receiptReportService, private route: ActivatedRoute) {
//    }

//    isActive: boolean = false;
//    garadeData: any;
//    today = new Date;
//    startDate: Date;
//    dateReport: any;

//    ngOnInit() {
//        this.today;
//        this.getGarageData();
//    }

//    stringDate(date) {
//        let d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(),
//            date.getMinutes(),
//            date.getSeconds()));
//        return d.toISOString().split('T')[0]
//    }

//    getGarageData() {
//        if (this.startDate) {
//            this.dateReport = this.stringDate(this.startDate);
//            console.log(this.dateReport)
//        }
//        else {
//            this.dateReport = null;
//        }

//        this.garage_receiptReportS.getAllGarageData(this.dateReport).subscribe(result => {
//            this.garadeData = result['data'];
//            console.log(JSON.stringify(this.garadeData))
//        }, error => {
//            console.log('error')
//        })
//    }
//    clearEndDate() {
//        this.startDate = null;
//        this.garadeData = [];
//    }

//    print(): void {
//        let printContents, popupWin;
//        printContents = document.getElementById('print-section').innerHTML;
//        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
//        popupWin.document.open();
//        popupWin.document.write(`
//      <html>
//        <head>
          
//          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap1.min.css"/>
//          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
//          <style>
//          //........Customized style.......
         
//          label {
//            display: inline-block;
//            margin-bottom: .5rem;
//          }
//          .col-md-12 {
//            width: 100%;
//            float: left;
//          }
        
//          .form-group {
//            margin-bottom: 1rem;
//          }
//          .text-center {
//            text-align: center!important;
//          }
//          .col-md-6 {
//            width: 50%;
//          }
//          .col-md-offset-3 {
//            margin-left: 25%;
//          }
//          .pull-right {
//            float: right!important;
//          }
//          .pull-left {
//            float: left!important;
//          }
//          .col-md-2 {
//            width: 16.66666667%;
//          }
//          .col-md-4 {
//            width: 33.33333333%;
//          }
//          div {
//            display: block;
//          }
       
//        .alert-info{
//          background: #555 !important;
//        }
//        .alert-info span{
//          color:#fff !important;
//        }
//        .table-bordered {
//            border: 1px solid #a3a4a5 !important;
//        }
//        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
//            border: 1px solid #797979 !important;
//        }
//        tr th{
//            font-weight: 600 !important;
//        }
//        h5{
//            font-weight: 600 !important;  
//        }
//        .table-bordered>thead>tr>th{
//          background: #555 !important;
//          color:#fff !important;
//        }
//        hr{
//          border-top: 1px solid #000 !important;
//          margin-top:-20px !important;
//        }
//        tr td hr{
//          margin-top:10px !important;
//        }

//            .table>thead>tr>th {
//              text-transform: capitalize;
//            }
//            * {
//              -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
//              color-adjust: exact !important;                 /*Firefox*/
//            }
//            .reportTitle{
//              margin-left:1px;
//              font-weight: 700;
//            }
//            .alert-info{
//              margin-top: 2%;
//          }
//          </style>
//        </head>
//    <body onload="window.print();window.close()">${printContents}</body>
//      </html>`
//        );
//        popupWin.document.close();
//    }

//}