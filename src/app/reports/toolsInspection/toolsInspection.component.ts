import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { VehicleService } from "app/vehicles/services/vehicle.service";

@Component({
  selector: "app-toolsInspectionReport",
  templateUrl: "./toolsInspection.component.html",
  styleUrls: ["./toolsInspetion.component.css"],
  providers: [VehicleService]
})
export class toolsInspectionReportComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private vehicleS: VehicleService
  ) {}

  isActive: boolean = false;
  types: any = [];

  ngOnInit() {
    this.getAllTypes();
  }

  getAllTypes() {
    this.vehicleS.getBytype().subscribe(
      result => {
        this.types = result["data"];
        console.log(JSON.stringify(this.types));
      },
      error => {
        console.log("error");
      }
    );
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById("print-section").innerHTML;
    popupWin = window.open("", "_blank", "top=0,left=0,height=100%,width=auto");
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          
          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap1.min.css"/>
          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
          <style>
          //........Customized style.......
         
          label {
            display: inline-block;
            margin-bottom: .5rem;
          }
          .col-md-12 {
            width: 100%;
            float: left;
          }
        
          .form-group {
            margin-bottom: 1rem;
          }
          .text-center {
            text-align: center!important;
          }
          .col-md-6 {
            width: 50%;
          }
          .col-md-offset-3 {
            margin-left: 25%;
          }
          .pull-right {
            float: right!important;
          }
          .pull-left {
            float: left!important;
          }
          .col-md-2 {
            width: 16.66666667%;
          }
          .col-md-4 {
            width: 33.33333333%;
          }
          div {
            display: block;
          }
       
        .alert-info{
          background: #555 !important;
        }
        .alert-info span{
          color:#fff !important;
        }
        .table-bordered {
            border: 1px solid #a3a4a5 !important;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #797979 !important;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
          border-top: none;
      }
        tr th{
            font-weight: 600 !important;
        }
        h5{
            font-weight: 600 !important;  
        }
        .table-bordered>thead>tr>th{
          background: #555 !important;
          color:#fff !important;
        }
        hr{
          border-top: 1px solid #000 !important;
          margin-top:-20px !important;
        }
        tr td hr{
          margin-top:10px !important;
        }

      input::-webkit-input-placeholder{
        display:inline;
      }
     
        * {
          -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
          color-adjust: exact !important;                 /*Firefox*/
        }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
