//import { Component, OnInit } from "@angular/core";
//import { IVehicle } from "../../shared/Interfaces/IVehicle.interface";
//import { CommonService } from "../../shared/services/common.service";
//import { jobCardService } from "../../shared/services/jobcard.service";
//import { serviceTypeService } from "../../shared/services/serviceType.service";
//import { vehicleService } from "../../shared/services/vehicle.service";

//@Component({
//    selector: "app-maintenanceReport",
//    templateUrl: "./maintenanceReport.component.html",
//    styleUrls: ["./maintenanceReport.component.css"],
//    providers: [jobCardService, vehicleService, serviceTypeService, CommonService]
//})// [M.Osman. This component is unused.]
//export class MaintenanceReportComponent implements OnInit {

//    constructor(
//        private vehicleService: vehicleService,
//        private jobCardService: jobCardService,
//        private serviceTypeService: serviceTypeService,
//        private commonService: CommonService) {
//    }

//    months: any = [
//        { id: 1, name: "January" },
//        { id: 2, name: "February" },
//        { id: 3, name: "March" },
//        { id: 4, name: "April" },
//        { id: 5, name: "May" },
//        { id: 6, name: "June" },
//        { id: 7, name: "July" },
//        { id: 8, name: "August" },
//        { id: 9, name: "September" },
//        { id: 10, name: "October" },
//        { id: 11, name: "November" },
//        { id: 12, name: "December" }];

//    fleetNum: string = "";
//    reportResult: any = [];
//    vehicles: IVehicle[];
//    serviceTypes: any = [];
//    firstRow: any = {};
//    isActive: boolean = false;

//    ngOnInit() {
//        this.isActive = true;

//        // get Vehicles
//        this.vehicleService.getVehicles().subscribe(
//            vehicleRes => {
//                if (vehicleRes["isSucceeded"] === true) {
//                    this.vehicles = vehicleRes["data"];
//                    this.isActive = false;
//                }

//                return this.vehicles;
//            });

//        this.getAllServiceTypes();
//    }

//    getAllServiceTypes() {
//        this.isActive = true;

//        this.serviceTypeService.getServiceTypes().subscribe(
//            result => {
//                this.serviceTypes = result["data"];
//                this.isActive = false;
//            },
//            () => {
//                console.log("error");
//            });
//    }

//    getPreventiveReportData(fleetNum) {

//        if (fleetNum === "") {
//            return;
//        }

//        this.isActive = true;

//        this.jobCardService.preventiveReport(fleetNum).subscribe(
//            result => {
//                this.reportResult = result;

//                if (this.reportResult && this.reportResult.length > 0) {
//                    this.firstRow = this.reportResult[0];
//                    // console.log(this.firstRow);
//                }

//                this.isActive = false;
//            },
//            () => {
//                console.log("error");
//            });
//    }

//    clearSearch() {
//        this.fleetNum = "";
//        this.reportResult = [];
//    }

//    print(): void {
//        this.commonService.print();
//    }
//}