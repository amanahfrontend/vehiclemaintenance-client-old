import { Component, OnInit } from "@angular/core";
import { GarageReceiptReportService } from "../../shared/services/garage-receipt-report.service";
import { CommonService } from "../../shared/services/common.service";
import { JobCardService } from "app/job-cards/services/job-card.service";

@Component({
    selector: "app-daily-service-report",
    templateUrl: "./dailyReceipt.component.html",
    styleUrls: ["./dailyReceipt.component.css"],
    providers: [JobCardService, GarageReceiptReportService, CommonService]
})
export class DailyReceiptReportComponent implements OnInit {

    constructor(
        private dailyReportService: GarageReceiptReportService,
        private commonService: CommonService) {
    }

    isActive: boolean = false;
    today = new Date;
    dailyReceipt: any = [];
    arrDailyReceipt: any = [];
    arrDailyBrought: any = [];
    body: any = [];

    // Search criteria for the component.
    dailyServiceSearchParams: any = {
        vehiclePlateNo: "",
        startDate: "",
        endDate: ""
    };

    ngOnInit() {
        this.dailyServiceSearchParams.startDate = new Date();
        this.dailyServiceSearchParams.endDate = new Date();

        // this.searchJobCards();
        // this.getDailyBrought();
        // this.getDailyReceiptExceptBrought();
    }

    getStartDate(): string {
        var startDate;

        if (this.dailyServiceSearchParams.startDate) {
            startDate = this.commonService.getDateAsString(this.dailyServiceSearchParams.startDate);
        } else {
            startDate = "";
        }

        return startDate;
    }

    getEndDate(): string {
        let endDate: string;

        if (this.dailyServiceSearchParams.endDate) {
            endDate = this.commonService.getDateAsString(this.dailyServiceSearchParams.endDate);
        } else {
            endDate = "";
        }

        return endDate;
    }

    searchJobCards(): void {
        const vehiclePlateNo = this.dailyServiceSearchParams.vehiclePlateNo;
        const startDate = this.getStartDate();
        const endDate = this.getEndDate();

        const dailyServiceSearchParams2 = {
            vehiclePlateNo: vehiclePlateNo,
            startDate: startDate,
            endDate: endDate
        };

        this.dailyReportService.getDailyReceiptReport(dailyServiceSearchParams2).subscribe(
            result => {
                this.arrDailyReceipt = result.data.filter(obj => obj.dailyServiceDesc === "Daily Receipt");
                this.arrDailyBrought = result.data.filter(obj => obj.dailyServiceDesc === "Brought Forward");
            },
            () => {
                console.log("error");
            });
    }

    //getDailyReceipt() {
    //    this.dailyReportService.getAllDailyReceipt().subscribe(
    //        result => {
    //            this.dailyReceipt = result["data"];
    //        },
    //        () => {
    //            console.log("error");
    //        });
    //}

    //getDailyBrought() {
    //    this.dailyReportService.getAllDailyBroughtReceipt(this.body).subscribe(
    //        result => {
    //            this.receiptsBrought = result["data"];
    //        },
    //        () => {
    //            console.log("error");
    //        });
    //}

    //getDailyReceiptExceptBrought() {

    //    this.dailyReportService.getAllDailyReceiptExceptBrought(this.body).subscribe(
    //        result => {
    //            this.receiptExceptBrought = result["data"];
    //        },
    //        () => {
    //            console.log("error");
    //        });
    //}

    clearSearch() {
        this.dailyServiceSearchParams.vehiclePlateNo = "";
        this.dailyServiceSearchParams.startDate = new Date();
        this.dailyServiceSearchParams.endDate = new Date();

        this.arrDailyReceipt = [];
        this.arrDailyBrought = [];
    }

    print(): void {
        const vehiclePlateNo = this.dailyServiceSearchParams.vehiclePlateNo;
        const startDate = this.getStartDate();
        const endDate = this.getEndDate();

        const queryStringParams = `vehiclePlateNo=${vehiclePlateNo}&startDate=${startDate}&endDate=${endDate}`;

        this.commonService.print2(3, queryStringParams);
    }
}
