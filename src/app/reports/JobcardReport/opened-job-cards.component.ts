import { Component, OnInit } from "@angular/core";
import { JobCardService } from "app/job-cards/services/job-card.service";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";

@Component({
    selector: "app_opened-job-cards",
    templateUrl: "./opened-job-cards.component.html",
    providers: [JobCardService]
})
export class OpenedJobCardsComponent implements OnInit {
    openJobCards: any = [];
    isActive: boolean = false;

    constructor(private jobCardService: JobCardService) { }

    ngOnInit() {
        this.getOpenJobCards();
    }

    getOpenJobCards() {
        this.isActive = true;

        this.jobCardService.getOpenJobCards().subscribe(
            schjobCardRes => {

                if (schjobCardRes["isSucceeded"] === true) {
                    this.openJobCards = schjobCardRes["data"];
                    this.isActive = false;
                }
            },
            () => {
                this.isActive = false;
            }
        );
    }
}
