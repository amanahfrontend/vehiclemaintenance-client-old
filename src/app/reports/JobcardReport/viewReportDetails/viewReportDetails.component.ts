import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { RepairRequestSparePartService } from "../../../repair-request-spare-parts/services/repair-request-spare-part.service";
import { RepairRequestService } from "../../../repair-requests/services/repair-request.service";
import { IJobCard_Technician } from "../../../shared/Interfaces/IJobCard_Technician.interface";
import { IRepairRequest } from "../../../shared/Interfaces/IRepairRequest.interface";
import { IUsedMaterials } from "../../../shared/Interfaces/IUsedMaterials.interface";
import { CommonService } from "../../../shared/services/common.service";
import { FileUploadService } from "../../../shared/services/file-upload.service";
import { JobCardService } from "app/job-cards/services/job-card.service";
import { JobCard } from "app/job-cards/models/job-card.model";


@Component({
    selector: "app-viewReportDetails",
    templateUrl: "./viewReportDetails.component.html",
    styleUrls: ["./viewReportDetails.component.css"],
    providers: [
        JobCardService, RepairRequestService, CommonService,
        RepairRequestSparePartService, FileUploadService
    ]
})
export class JobCardReportDetailsComponent implements OnInit {

    todayDate: Date = new Date();
    jobCard: JobCard[];
    usedMaterials: IUsedMaterials[];
    jobCardTechnicians: IJobCard_Technician[];
    jobCardId: any = this.route.snapshot.params["id"];
    id: number;
    model: any = {};
    isActive: boolean = false;
    repairRequests: IRepairRequest[];
    spareParts: IUsedMaterials[];
    repairRequestIds: any = [];
    uploadedFiles: any[] = [];

    constructor(
        private repairRequestService: RepairRequestService,
        private jobCardService: JobCardService,
        private sparePartsRequestService: RepairRequestSparePartService,
        private commonService: CommonService,
        private fileUploadService: FileUploadService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {

        this.route.params.subscribe(params => {
            this.id = +params["id"];
            this.jobCardId = this.id;
        });

        this.getJobCardData();
        this.getRepairRequestsByJobCard();
        this.getSparePartsOfJobCard();
        this.getUploadedFilesOfJobCard();
    }

    getJobCardData() {

        this.jobCardService.getJobCardById(this.id).subscribe(
            jobCardRes => {
                if (jobCardRes["isSucceeded"] === true) {
                    this.model = jobCardRes["data"];
                    this.isActive = false;
                }
            });
    }

    getRepairRequestsByJobCard() {

        this.repairRequestService.getByJobCardId(this.jobCardId).subscribe(
            repairRequestRes => {
                this.isActive = true;

                if (repairRequestRes["isSucceeded"] === true) {
                    this.repairRequests = repairRequestRes["data"];
                    this.isActive = false;
                }
            });
    }

    getSparePartsOfJobCard() {

        this.sparePartsRequestService.getRepairRequestSparePartsOfJobCard(this.jobCardId).subscribe(
            repairRequestRes => {
                this.isActive = true;

                if (repairRequestRes["isSucceeded"] === true) {
                    this.spareParts = repairRequestRes["data"];
                    this.isActive = false;
                }
            });

        //this.repairRequestservice.getByJobCardId(this.jobCardId).subscribe(
        //    repairrequestRes => {
        //        this.isActive = true;
        //        this.repairRequests = repairrequestRes["data"];

        //        for (let i = 0; i < this.repairRequests.length; i++) {
        //            this.repairRequestIds.push(this.repairRequests[i].id);
        //        }

        //        this.isActive = false;

        //        // this.commonService.log(this.repairRequestIds);

        //        this.usedmaterialsservice.getByRepairRequestIDs(this.repairRequestIds).subscribe(
        //            result => {
        //                this.isActive = true;

        //                if (result["isSucceeded"] === true) {
        //                    this.spareParts = result["data"];
        //                    this.isActive = false;
        //                }
        //            });
        //    });
    }

    getUploadedFilesOfJobCard() {

        this.fileUploadService.getUploadedFilesByJobCard(this.id).subscribe(
            responseResult => {
                if (responseResult["isSucceeded"] === true) {
                    this.uploadedFiles = responseResult["data"];
                }
            });
    }

    print(): void {
        const queryStringParams = `jobCardId=${this.jobCardId}`;
        this.commonService.print2(2, queryStringParams);
    }
}