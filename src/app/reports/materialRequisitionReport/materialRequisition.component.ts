import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../shared/services/common.service";

@Component({
    selector: "app-materialRequisitionReport",
    templateUrl: "./materialRequisition.component.html",
    styleUrls: ["./materialRequisition.component.css"],
    providers: [CommonService]
})
export class MaterialRequisitionReportComponent implements OnInit {

    constructor(private commonService: CommonService) {
    }

    //isActive: boolean = false;
    //allRepairRequests: any = [];
    //reportResult: any;
    //repairRequestId: number = 0;

    ngOnInit() {
        //this.getAllRepairRequest();
    }

    //getAllRepairRequest() {
    //    this.isActive = true;

    //    this.repairRequestS.getRepairRequests().subscribe(
    //        result => {
    //            this.allRepairRequests = result["data"];
    //            // console.log(JSON.stringify(this.allRepairRequests));
    //            this.isActive = false;
    //        },
    //        () => {
    //            console.log("error");
    //        });
    //}

    //getMaterialRequestionReportData() {
    //    if (this.repairRequestId === 0) {
    //        return;
    //    }

    //    this.isActive = true;

    //    this.repairRequestS.materialRequestionReport(this.repairRequestId).subscribe(
    //        result => {
    //            this.reportResult = result["data"];
    //            this.isActive = false;
    //            // console.log(JSON.stringify(this.reportResult));
    //        },
    //        () => {
    //            console.log("error");
    //        });
    //}

    //clearSearch() {
    //    this.repairRequestId = 0;
    //    this.reportResult = [];
    //}

    printEmptyMaterialRequisitionReport(): void {
        // 6 =  MaterialRequisitionReport.rdl
        this.commonService.print2(6, "deliveryOrderId=0");
    }
}
