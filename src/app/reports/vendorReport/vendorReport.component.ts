import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators/map';
import { startWith } from 'rxjs/operators/startWith';
import { VendorsService } from "../../vendors/services/vendors.service";

@Component({
    selector: 'app-sparePartsRequestReport',
    templateUrl: './vendorReport.component.html',
    styleUrls: ['./vendorReport.component.css'],
    providers: [VendorsService]
})
export class vendorReportReportComponent implements OnInit {


    constructor(_router: Router, private vendorS: VendorsService, private route: ActivatedRoute) {
    }

    myControl: FormControl = new FormControl();
    isActive: boolean = false;
    vendors: any = [];
    reportResult: any;
    vendorId: any;
    filteredOptions: any = [];
    dataModel: any = {};
    config = {
        displayKey: "name",
        search: true
    }


    ngOnInit() {

        this.myControl.valueChanges.pipe(startWith(''), map(val => this.filterValues(val)));

        this.getAllVendors();
    }

    filterValues(search: string) {
        return this.vendors.filter(value => {
            console.log(value);
            value.name_ER.toLowerCase().indexOf(search.toLowerCase()) > -1;
        });
    }

    getAllVendors() {
        this.vendorS.getVendors().subscribe(result => {
            if (result["isSucceeded"] == true) {
                this.vendors = result["data"];
                // this.vendors.map(vendorResult =>{
                //     this.dataModel = vendorResult.id;
                //     console.log(this.dataModel.id)
                // })
                this.isActive = false;
                console.log(this.vendors)
            }
            return this.vendors;

        });
    }

    getVendorsReportData(vendorId) {
        this.isActive = true;
        console.log(vendorId)
        this.vendorS.vendorReport(vendorId).subscribe(result => {
            this.reportResult = result['data'];
            this.isActive = false;
            console.log(JSON.stringify(this.reportResult))
        }, error => {
            console.log('error')
        })
    }

    clearShearch() {
        this.vendorId = '';
        this.reportResult = [];
    }

    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          
          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap1.min.css"/>
          <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
          <style>
          //........Customized style.......
         
          label {
            display: inline-block;
            margin-bottom: .5rem;
          }
          .col-md-12 {
            width: 100%;
            float: left;
          }
        
          .form-group {
            margin-bottom: 1rem;
          }
          .text-center {
            text-align: center!important;
          }
          .col-md-6 {
            width: 50%;
          }
          .col-md-offset-3 {
            margin-left: 25%;
          }
          .pull-right {
            float: right!important;
          }
          .pull-left {
            float: left!important;
          }
          .col-md-2 {
            width: 16.66666667%;
          }
          .col-md-4 {
            width: 33.33333333%;
          }
          div {
            display: block;
          }
       
        .alert-info{
          background: #555 !important;
        }
        .alert-info span{
          color:#fff !important;
        }
        .table-bordered {
            border: 1px solid #a3a4a5 !important;
        }
        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
            border: 1px solid #797979 !important;
        }
        tr th{
            font-weight: 600 !important;
        }
        h5{
            font-weight: 600 !important;  
        }
        .table-bordered>thead>tr>th{
          background: #555 !important;
          color:#fff !important;
        }
        hr{
          border-top: 1px solid #000 !important;
          margin-top:-20px !important;
        }
        tr td hr{
          margin-top:10px !important;
        }
        .approveSign{
          height: 80px;
        }
        .note .sign{
          margin-left:-15px;
        }
            .table>thead>tr>th {
              text-transform: capitalize;
            }
            * {
              -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
              color-adjust: exact !important;                 /*Firefox*/
            }
            .reportTitle{
              margin-left:-8px;
              font-weight: 700;
            }
            .alert-info{
              margin-top: 2%;
          }
          .note p{
            margin-left:15px;
          }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
        );
        popupWin.document.close();
    }


}