import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../shared/services/common.service";
import { RepairRequestService } from "../../repair-requests/services/repair-request.service";

@Component({
    selector: "app-internal-purchase-request",
    templateUrl: "./internal-purchase-request.component.html",
    styleUrls: ["./internal-purchase-request.component.css"],
    providers: [RepairRequestService, CommonService]
})
export class InternalPurchaseRequestComponent implements OnInit {
    constructor(
        private repairRequestS: RepairRequestService,
        private commonService: CommonService) {
    }

    isActive: boolean = false;
    allSparts: any = [];
    reportResult: any = {};
    sparePartRequestId: number = 0;

    ngOnInit() {
        this.getAllSpareParts();
    }

    getAllSpareParts() {

        this.repairRequestS.getAllInternalSpares().subscribe(
            result => {
                this.allSparts = result["data"];
                console.log(JSON.stringify(this.allSparts));
            },
            () => {
                console.log("error");
            }
        );
    }

    getPayRequestReportData() {
        if (this.sparePartRequestId === 0) {
            return;
        }

        this.isActive = true;

        this.repairRequestS.payRequestReport(this.sparePartRequestId).subscribe(
            result => {
                this.reportResult = result.data;
                this.isActive = false;
                // console.log(JSON.stringify(this.reportResult));
            },
            () => {
                console.log("error");
            }
        );
    }

    clearSearch() {
        this.sparePartRequestId = 0;
        this.reportResult = [];
    }

    getPayRequestReport() {
        // this.repairRequestS.payRequestReport()
    }

    print(): void {
        this.commonService.print();
    }
}
