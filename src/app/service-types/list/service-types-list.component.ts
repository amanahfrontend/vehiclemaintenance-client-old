import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { IServiceType } from "../../shared/Interfaces/IServiceType.interface";
import { DialogService } from "../../shared/services/dialog.service";
import { ServiceTypeService } from "../services/service-type.service";
import { ServiceTypeAddEditComponent } from "../add-edit/service-type-add-edit.component";

@Component({
    selector: "app-service-types-list",
    templateUrl: "./service-types-list.component.html",
    styleUrls: ["./service-types-list.component.scss"],
    providers: [ServiceTypeService]
})
export class ServiceTypesListComponent implements OnInit {
    serviceTypes: IServiceType[];
    isActive: boolean = false;

    constructor(
        private serviceTypeService: ServiceTypeService,
        private customDialogService: DialogService,
        public dialog: MatDialog
    ) {
    }

    ngOnInit() {
        this.getServicesTypes();
    }

    getServicesTypes() {
        this.isActive = true;

        this.serviceTypeService.getServiceTypes().subscribe(
            serviceTypeRes => {
                if (serviceTypeRes["isSucceeded"] === true) {
                    this.serviceTypes = serviceTypeRes["data"];

                    this.isActive = false;
                }
            });
    }

    openAddEditServiceTypeDialog(serviceTypeId: number): void {

        const dialogOptions = {
            width: "550px",
            data: { id: serviceTypeId }
        };

        const dialogRef = this.dialog.open(ServiceTypeAddEditComponent, dialogOptions);

        dialogRef.afterClosed().subscribe(() => {
            this.getServicesTypes();
        });
    }

    deleteServiceType(id) {
        const question = "Do you really want to delete this record?";
        const result = this.customDialogService.confirm("Confirm", question, true);

        result.subscribe(
            res => {

                if (res) {
                    this.serviceTypeService.deleteServiceType(id).subscribe(
                        serviceTypeRes => {

                            if (serviceTypeRes["isSucceeded"] === true) {
                                this.getServicesTypes();
                            }
                        });
                }
            },
            () => {
                console.log("Declined");
            }
        );
    }
}
