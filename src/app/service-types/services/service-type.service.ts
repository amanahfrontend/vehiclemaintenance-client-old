import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/observable";
import { IServiceType } from "../../shared/Interfaces/IServiceType.interface";
import { WebService } from "../../shared/services/webService";

@Injectable()
export class ServiceTypeService {
    constructor(private http: HttpClient, private config: WebService) { }

    public getServiceTypes(): Observable<IServiceType[]> {
        const url = this.config.baseUrl + "ServiceType/Get";
        return this.http.get(url).map(data => {
            const result = <IServiceType[]>data;
            return result;
        });
    }

    public getServiceType(id: number): Observable<any> {

        const url = this.config.baseUrl + "ServiceType/Get/" + id;

        return this.http.get(url).map(data => {
            const result = <any>data;
            return result;
        });
    }

    addServiceType(model): Observable<IServiceType> {
        model.rowStatusId = 1;
        model.createdByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "ServiceType/Add";

        return this.http.post(url, model).map(
            data => {
                const result = <IServiceType>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    updateServiceType(model): Observable<IServiceType> {
        model.rowStatusId = 1;
        model.modifiedByUserId = this.config.currentUserId;

        const url = this.config.baseUrl + "ServiceType/Update";

        return this.http.put(url, model).map(
            data => {
                const result = <IServiceType>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }

    deleteServiceType(id: number) {
        let url = "{0}ServiceType/Delete/{1}/{2}";

        url = url.replace("{0}", this.config.baseUrl).replace("{1}", id.toString())
            .replace("{2}", this.config.currentUserId);

        return this.http.delete(url).map(
            data => {
                const result = <IServiceType>data;
                return result;
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        );
    }
}
