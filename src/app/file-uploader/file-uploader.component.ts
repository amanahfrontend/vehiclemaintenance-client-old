import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FileUploadService } from "../shared/services/file-upload.service";
import Uploadedfilemodel = require("./uploaded-file.model");
import UploadedFile = Uploadedfilemodel.UploadedFile;

@Component({
    selector: "app-file-uploader",
    templateUrl: "./file-uploader.component.html",
    providers: [FileUploadService]
})
export class FileUploaderComponent implements OnInit {
    @Output() uploadingComplete = new EventEmitter();

    @Output()
    get uploadedFiles(): UploadedFile[] {
        const arrUploadedFiles: UploadedFile[] = [];

        if (this.selectedFiles === undefined) {
            return arrUploadedFiles;
        }

        for (let index = 0; index < this.selectedFiles.length; index++) {

            const uploadedFile = {
                fileName: this.selectedFiles[index].name,
                fileType: this.selectedFiles[index].type
            };

            arrUploadedFiles.push(uploadedFile);
        }

        return arrUploadedFiles;
    }

    selectedFiles: FileList;

    constructor(private fileUploadService: FileUploadService) { }

    ngOnInit() {
    }

    onFileSelected(files: FileList) {
        this.selectedFiles = files;
    }

    uploadButtonClick() {
        //const filePaths: string[] = [];

        //for (let i = 0; i < this.selectedFiles.length; i++) {

        //    filePaths.push("file.jpg");
        //}

        //this.uploadFiles(filePaths);
    }

    uploadFiles(filePaths: string[]) {

        this.fileUploadService.postFiles(this.selectedFiles, filePaths).subscribe(
            responseResult => {
                if (responseResult["isSucceeded"] === true) {
                    this.uploadingComplete.emit();
                } else {
                    console.log("Uploading files failed. " + responseResult.message);
                }
            });
    }
}
