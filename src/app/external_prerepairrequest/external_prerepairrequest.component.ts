import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { IRepairRequest } from '../shared/Interfaces/IRepairRequest.interface';
import { RepairRequestService } from '../repair-requests/services/repair-request.service';

@Component({
    selector: 'app-external-prerepairrequest',
    templateUrl: './external_prerepairrequest.component.html',
    styleUrls: ['./external_prerepairrequest.component.css'],
    providers: [RepairRequestService]
})

export class External_PreRepairRequestComponent implements OnInit {
    repairrequests: IRepairRequest[];
    type: boolean;

    constructor(private repairrequestservice: RepairRequestService) { }
    ngOnInit() {
        this.type = true;
        
        this.repairrequestservice.getByExternalandMaintenanceType(true, this.type).subscribe(
            repairrequestRes => {
                if (repairrequestRes["isSucceeded"] === true) {
                    this.repairrequests = repairrequestRes["data"];
                    console.log(this.repairrequests);
                }
                return this.repairrequests;
            });
    }

}