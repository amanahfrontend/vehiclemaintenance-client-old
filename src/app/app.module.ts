import { MdlModule } from "@angular-mdl/core";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MatAutocompleteModule, MatButtonModule, MatCardModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatInputModule, MatNativeDateModule, MatRadioModule, MatTabsModule, MAT_DATE_LOCALE } from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChartModule } from "angular-highcharts";
// import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
//import { DashboardModule } from "./dashboard/dashboard.module";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { ChartsModule } from "ng4-charts/ng4-charts";
import { SelectDropDownModule } from "ngx-select-dropdown";
import { AddSchRepairRequestComponent } from "./addschrepairrequest/addschrepairrequest.component";
import { AddUsedMaterialsComponent } from "./addusedMaterials/addusedMaterials.component";
import { AddVendorToRepairRequestComponent } from "./addvendortorepairrequest/add-vendor-to-repair-request.component";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.routing";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DriverAddComponent } from "./drivers/add/driver-add.component";
import { DriverEditComponent } from "./drivers/edit/driver-edit.component";
import { DriversListComponent } from "./drivers/list/drivers-list.component";
import { External_PreRepairRequestComponent } from "./external_prerepairrequest/external_prerepairrequest.component";
import { External_RepairRequestDetailsComponent } from "./external_repairrequestdetails/external_repairrequestdetails.component";
import { External_SchRepairRequestComponent } from "./external_schrepairrequest/external_schrepairrequest.component";
import { FileUploaderComponent } from "./file-uploader/file-uploader.component";
import { HomeComponent } from "./home/home.component";
import { Internal_PreRepairRequestComponent } from "./internal_prerepairrequest/internal_prerepairrequest.component";
import { Internal_SchRepairRequestComponent } from "./internal_schrepairrequest/internal_schrepairrequest.component";
import { JobCardAddComponent } from "./job-cards/add/job-card-add.component";
import { LbdModule } from "./lbd/lbd.module";
import { NonApprovedRequestSparePartsComponent } from "./nonapprovedrequestspareparts/nonapprovedrequestspareparts.component";
import { NonApprovedRequestSparePartsDetailsComponent } from "./nonapprovedrequestsparepartsdetails/nonapprovedrequestsparepartsdetails.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { preJobCardComponent } from "./prejobcard/prejobcard.component";
import { PreventiveMaintenanceComponent } from "./preventivemaintenance/preventivemaintenance.component";
import { preventiveRepiarRequestComponent } from "./preventiveRepairRequest/preventiveRepiarRequest.component";
import { RegisterComponent } from "./Register/register.component";
import { RepairRequestDetailsTabsComponent } from "./repair-request-details/repair-request-details-tabs/repair-request-details-tabs.component";
import { RepairRequestSparePartsListComponent } from "./repair-request-spare-parts/list/repair-request-spare-parts-list.component";
import { RepairRequestStatusAddComponent } from "./repair-request-status/add/repair-request-status-add.component";
import { RepairRequestStatusEditComponent } from "./repair-request-status/edit/repair-request-status-edit.component";
import { RepairRequestStatusListComponent } from "./repair-request-status/list/repair-request-status-list.component";
import { RepairRequestTypeAddComponent } from "./repair-request-type/add/repair-request-type-add.component";
import { RepairRequestTypeEditComponent } from "./repair-request-type/edit/repair-request-type-edit.component";
import { RepairRequestTypeListComponent } from "./repair-request-type/list/repair-request-type-list.component";
import { RepairRequestAddComponent } from "./repair-requests/add/repair-request-add.component";
import { RepairRequestsPendingDetailsComponent } from "./repair-requests/pending-details/repair-requests-pending-details.component";
import { RepairRequestsPendingComponent } from "./repair-requests/pending/repair-requests-pending.component";
import { RepairRequestsSparePartsReportComponent } from "./repair-request-spare-parts/report/repair-requests-spare-parts-report.component";
import { DailyReceiptReportComponent } from "./reports/dailyReceiptReport/dailyReceipt.component";
import { equipmentReportComponent } from "./reports/equipmentReport/equipment.component";
import { InternalPurchaseRequestComponent } from "./reports/internal-purchase-request/internal-purchase-request.component";
import { OpenedJobCardsComponent } from "./reports/JobcardReport/opened-job-cards.component";
import { JobCardReportDetailsComponent } from "./reports/JobcardReport/viewReportDetails/viewReportDetails.component";
import { MaterialRequisitionReportComponent } from "./reports/materialRequisitionReport/materialRequisition.component";
import { ospOrdersReportComponent } from "./reports/ospWorkOrdersReport/ospOrders.component";
import { toolsInspectionReportComponent } from "./reports/toolsInspection/toolsInspection.component";
import { vendorReportReportComponent } from "./reports/vendorReport/vendorReport.component";
import { RoleAddComponent } from "./roles/add/role-add.component";
import { RoleEditComponent } from "./roles/edit/role-edit.component";
import { RolesListComponent } from "./roles/list/roles-list.component";
import { ScheduledMaintenanceComponent } from "./scheduledmaintenance/scheduledmaintenance.component";
import { schJobCardComponent } from "./schjobcard/schjobcard.component";
import { SchJobCardDetailsComponent } from "./SchjobcardDetails/SchjobcardDetails.component";
import { sechedualRepiarRequestComponent } from "./sechedualRepiarRequest/sechedualRepiarRequest.component";
import { ServiceTypeAddEditComponent } from "./service-types/add-edit/service-type-add-edit.component";
import { ServiceTypesListComponent } from "./service-types/list/service-types-list.component";
import { ConfirmDialogComponent } from "./shared/components/confirm-dialog/confirm-dialog.component";
import { PaginationComponent } from "./shared/components/pagination/pagination.component";
import { FooterModule } from "./shared/footer/footer.module";
import { NavbarModule } from "./shared/navbar/navbar.module";
import { FilterQueryPipe } from "./shared/pipes/filter-query.pipe";
import { AuthGuard } from "./shared/services/AuthGuard.service";
import { DialogService } from "./shared/services/dialog.service";
import { WebService } from "./shared/services/webService";
import { SidebarModule } from "./sidebar/sidebar.module";
import { DeliveryOrderListComponent } from "./spare-parts-delivery-orders/delivery-order-list/delivery-order-list.component";
import { DeliveryOrderNewComponent } from "./spare-parts-delivery-orders/delivery-order-new/delivery-order-new.component";
import { SparePartAddEditComponent } from "./spare-parts/add-edit/spare-part-add-edit.component";
import { SparePartsListComponent } from "./spare-parts/list/spare-parts-list.component";
import { TechnicianAddComponent } from "./technicians/add/technician-add.component";
import { TechnicianEditComponent } from "./technicians/edit/technician-edit.component";
import { TechniciansListComponent } from "./technicians/list/technicians-list.component";
import { TechnicianManpowerComponent } from "./technicians/man-power/technician-man-power.component";
import { TechniciansTabsComponent } from "./technicians/tabs/technicians-tabs.component";
import { TechniciansWorkReportComponent } from "./technicians/work-report/technicians-work-report.component";
import { UpdateattachmentsComponent } from "./updateattachments/updateattachments.component";
import { UpdatePreJobCardComponent } from "./updateprejobcard/updateprejobcard.component";
import { UpdateRepairRequestComponent } from "./updaterepairrequest/updaterepairrequest.component";
import { UpdateSchJobCardComponent } from "./updateschjobcard/updateschjobcard.component";
import { UserAddComponent } from "./users/add/user-add.component";
import { UserChangePasswordComponent } from "./users/change-password/user-change-password.component";
import { UserEditComponent } from "./users/edit/user-edit.component";
import { UsersListComponent } from "./users/list/users-list.component";
import { LoginComponent } from "./users/login/login.component";
import { UserProfileEditComponent } from "./users/profile-edit/user-profile-edit.component";
import { VehicleTypeAddEditComponent } from "./vehicle-types/add-edit/vehicle-type-add-edit.component";
import { VehicleTypesListComponent } from "./vehicle-types/list/vehicle-types-list.component";
import { VehicleAddEditComponent } from "./vehicles/add-edit/vehicle-add-edit.component";
import { VehicleDetailsTabsComponent } from "./vehicles/details-tabs/vehicle-details-tabs.component";
import { VehicleDetails2Component } from "./vehicles/details2/vehicle-details2.component";
import { VehiclesListComponent } from "./vehicles/list/vehicles-list.component";
import { VendorAddEditComponent } from "./vendors/add-edit/vendor-add-edit.component";
import { VendorsListComponent } from "./vendors/list/vendors-list.component";
import { JobCardRepairRequestSearchCriteriaComponent } from './job-card-repair-request-search-criteria/job-card-repair-request-search-criteria.component';
import { JobCardsListReportComponent } from './job-cards/list-report/job-cards-list-report.component';

@NgModule({
    declarations: [
        AppComponent,
        VehicleAddEditComponent,
        JobCardAddComponent,
        RepairRequestAddComponent,
        HomeComponent,
        DriversListComponent,
        DriverAddComponent,
        RoleAddComponent,
        UsersListComponent,
        RegisterComponent,
        LoginComponent,
        NotificationsComponent,
        DashboardComponent,
        ServiceTypeAddEditComponent,
        ServiceTypesListComponent,
        schJobCardComponent,
        RolesListComponent, RoleEditComponent,
        Internal_PreRepairRequestComponent,
        External_PreRepairRequestComponent,
        Internal_SchRepairRequestComponent,
        External_SchRepairRequestComponent,
        RepairRequestStatusListComponent,
        AddSchRepairRequestComponent,
        SchJobCardDetailsComponent,
        External_RepairRequestDetailsComponent,
        UpdatePreJobCardComponent,
        UpdateSchJobCardComponent,
        UpdateRepairRequestComponent,
        UpdateattachmentsComponent,
        DriverEditComponent,
        PreventiveMaintenanceComponent,
        ScheduledMaintenanceComponent,
        UserEditComponent,
        RepairRequestsPendingDetailsComponent,
        preJobCardComponent,
        AddUsedMaterialsComponent,
        NonApprovedRequestSparePartsComponent,
        NonApprovedRequestSparePartsDetailsComponent,
        SparePartAddEditComponent,
        SparePartsListComponent,
        JobCardReportDetailsComponent,
        ConfirmDialogComponent,
        sechedualRepiarRequestComponent,
        preventiveRepiarRequestComponent,
        FilterQueryPipe,
        TechniciansTabsComponent,
        TechnicianManpowerComponent,
        TechniciansListComponent,
        TechnicianAddComponent,
        TechnicianEditComponent,
        VehicleDetailsTabsComponent,
        ospOrdersReportComponent,
        toolsInspectionReportComponent,
        equipmentReportComponent,
        DailyReceiptReportComponent,
        MaterialRequisitionReportComponent,
        InternalPurchaseRequestComponent,
        RepairRequestsSparePartsReportComponent,
        vendorReportReportComponent,
        VehicleTypeAddEditComponent,
        VehicleTypesListComponent,
        OpenedJobCardsComponent,
        PaginationComponent,
        UserChangePasswordComponent,
        VehicleDetails2Component,
        FileUploaderComponent,
        AddVendorToRepairRequestComponent,
        VendorsListComponent,
        VendorAddEditComponent,
        RepairRequestsPendingComponent,
        RepairRequestStatusAddComponent,
        RepairRequestStatusEditComponent,
        RepairRequestTypeListComponent,
        RepairRequestTypeAddComponent,
        RepairRequestTypeEditComponent,
        UserAddComponent,
        RepairRequestSparePartsListComponent,
        DeliveryOrderNewComponent,
        DeliveryOrderListComponent,
        RepairRequestDetailsTabsComponent,
        UserProfileEditComponent,
        TechniciansWorkReportComponent,
        VehiclesListComponent,
        JobCardRepairRequestSearchCriteriaComponent,
        JobCardsListReportComponent
    ],

    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        HttpModule,
        NavbarModule,
        FooterModule,
        SidebarModule,
        RouterModule,
        AppRoutingModule,
        MdlModule,
        LbdModule,
        NgbModule.forRoot(),
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatTabsModule,
        MatCardModule,
        MatAutocompleteModule,
        MatInputModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatRadioModule,
        ChartsModule,
        ChartModule,
        MatDialogModule,
        SelectDropDownModule
    ],

    entryComponents: [
        ConfirmDialogComponent,
        VehicleAddEditComponent,
        JobCardAddComponent,
        RepairRequestAddComponent,
        UserAddComponent,
        DriverAddComponent,
        RepairRequestStatusAddComponent,
        RepairRequestTypeAddComponent,
        RoleAddComponent,
        SparePartAddEditComponent,
        VehicleTypeAddEditComponent,
        TechnicianAddComponent,
        ServiceTypeAddEditComponent,
        VendorAddEditComponent
    ],

    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        { provide: MAT_DATE_LOCALE, useValue: "en-GB" },
        WebService, AuthGuard, DialogService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
