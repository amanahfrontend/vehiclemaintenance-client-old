import { MdlDialogService } from "@angular-mdl/core";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import { RepairRequestVendor } from "../repair-requests/models/repair-request-vendor.model";
import { RepairRequestVendorService } from "../repair-requests/services/repair-request-vendor.service";
import { IRepairRequestStatus } from "../repair-request-status/models/IRepairRequestStatus.interface";
import { RepairRequestType } from "../repair-request-type/models/repair-request-type.model";
import { IServiceType } from "../shared/Interfaces/IServiceType.interface";
import { RepairRequestTypeService } from "../repair-request-type/services/repair-request-type.service";
import { RepairRequestService } from "../repair-requests/services/repair-request.service";
import { RepairRequestStatusService } from "../repair-request-status/services/repair-request-status.service";
import { ServiceTypeService } from "../service-types/services/service-type.service";

@Component({
    selector: "app-updaterepairrequest",
    templateUrl: "./updaterepairrequest.component.html",
    styleUrls: ["./updaterepairrequest.component.css"],
    providers: [
        RepairRequestService, ServiceTypeService,
        RepairRequestStatusService, RepairRequestTypeService, RepairRequestVendorService
    ]
})
export class UpdateRepairRequestComponent implements OnInit {
    @Output() onSuccessfulUpdate = new EventEmitter();

    visible = false;
    visibleAnimate = false;
    model: any = {};
    repairRequestTypes: RepairRequestType[];
    repairRequestStatuses: IRepairRequestStatus[];
    serviceTypes: IServiceType[];
    isActive: boolean = false;
    repairTypeId: string;
    repairRequestVendors: RepairRequestVendor[] = [];
    repairRequestId: number;

    constructor(
        private dialogService: MdlDialogService,
        private serviceTypeService: ServiceTypeService,
        private repairRequestStatusService: RepairRequestStatusService,
        private repairRequestTypeService: RepairRequestTypeService,
        private repairRequestVendorService: RepairRequestVendorService,
        private repairRequestService: RepairRequestService) {

    }

    ngOnInit() {

        this.getServiceTypes();

        this.getRepairRequestStatuses();

        this.getRepairRequestTypes();
    }

    getServiceTypes() {
        // Get service types
        this.serviceTypeService.getServiceTypes().subscribe(
            serviceTypeRes => {
                if (serviceTypeRes["isSucceeded"] === true) {
                    this.serviceTypes = serviceTypeRes["data"];
                }
            });
    }

    getRepairRequestStatuses() {
        // Get repair request statuses.
        this.repairRequestStatusService.getRepairRequestStatus().subscribe(
            repairRequestStatusRes => {
                if (repairRequestStatusRes["isSucceeded"] === true) {
                    this.repairRequestStatuses = repairRequestStatusRes["data"];
                }
            });
    }

    getRepairRequestTypes() {
        // get repair request types.
        this.repairRequestTypeService.getRepairRequestTypes().subscribe(
            repairRequestTypesRes => {
                if (repairRequestTypesRes["isSucceeded"] === true) {
                    this.repairRequestTypes = repairRequestTypesRes["data"];
                }
            });
    }

    getRepairRequestVendors(repairRequestId: number) {
        this.repairRequestId = repairRequestId;

        this.repairRequestVendorService.getByRepairRequest(repairRequestId).subscribe(
            responseResult => {
                if (responseResult["isSucceeded"] === true) {
                    this.repairRequestVendors = responseResult.data;
                }
            }
        );
    }

    repairRequestVendorsAttachedFilesChanged() {
        this.getRepairRequestVendors(this.repairRequestId);
    }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    showDialog(model) {
        window.scroll(0, 0);

        this.repairRequestService.getRepairRequestById(model.id).subscribe(
            repairRequestRes => {
                this.isActive = true;

                if (repairRequestRes["isSucceeded"] === true) {
                    this.model = repairRequestRes["data"];
                    this.isActive = false;

                    if (this.model.external) {
                        this.getRepairRequestVendors(model.id);
                    }
                }
            });

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    updateRepairRequest() {

        this.isActive = true;

        this.repairRequestService.updateRepairRequest(this.model).subscribe(
            repairRequestRes => {
                if (repairRequestRes["isSucceeded"] === true) {
                    this.isActive = false;
                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);

                    this.hide();
                    this.onSuccessfulUpdate.emit();
                } else {
                    this.dialogService.alert("Something wrong");
                    console.log(repairRequestRes.message);
                }
            });
    }
}
