import { Component, OnInit } from "@angular/core";
import { RepairRequestStatusService } from "app/repair-request-status/services/repair-request-status.service";
import { RepairRequestType } from "../repair-request-type/models/repair-request-type.model";
import { IServiceType } from "../shared/Interfaces/IServiceType.interface";
import { CommonVariables } from "../shared/services/common-variables.service";
import { CommonService } from "../shared/services/common.service";
import { DialogService } from "../shared/services/dialog.service";
import { RepairRequestTypeService } from "../repair-request-type/services/repair-request-type.service";
import { RepairRequestService } from "../repair-requests/services/repair-request.service";
import { ServiceTypeService } from "../service-types/services/service-type.service";

@Component({
    selector: "app-addschrepairrequest",
    templateUrl: "./addschrepairrequest.component.html",
    styleUrls: ["./addschrepairrequest.component.css"],
    providers: [
        RepairRequestService, CommonVariables, ServiceTypeService,
        RepairRequestStatusService, RepairRequestTypeService, CommonService
    ]
})
export class AddSchRepairRequestComponent implements OnInit {
    visible = false;
    visibleAnimate = false;
    model: any = {};
    model2: any = {};
    repairRequestTypes: RepairRequestType[];
    serviceTypes: IServiceType[];
    type: boolean;
    date: any = new Date;
    startTime: any = new Date;
    isActive: boolean = false;

    constructor(
        private customDialogService: DialogService,
        private repairRequestTypeService: RepairRequestTypeService,
        private repairRequestService: RepairRequestService,
        private commonS: CommonVariables,
        private serviceTypeservice: ServiceTypeService,
        private commonService: CommonService) {

    }

    ngOnInit() {

        this.type = false;

        // Get Service Types
        this.serviceTypeservice.getServiceTypes().subscribe(
            serviceTypeRes => {
                if (serviceTypeRes["isSucceeded"] === true) {
                    this.serviceTypes = serviceTypeRes["data"];
                }
            });

        // Get Repair Request Types
        this.repairRequestTypeService.getRepairRequestTypes().subscribe(
            repairRequestTypesRes => {
                if (repairRequestTypesRes["isSucceeded"] === true) {
                    this.repairRequestTypes = repairRequestTypesRes["data"];
                }
            });
    }

    showDialog(jobCardId): void {
        this.model = {};
        this.commonS.jobCardId = jobCardId;

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    hide(): void {
        this.model = {};
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains("modal")) {
            this.hide();
        }
    }

    saveRepairRequest() {

        this.isActive = true;
        this.model.jobCardId = this.commonS.jobCardId;
        this.model.userId = sessionStorage.getItem("id");
        this.date = this.commonService.getDateAsString(this.date);

        this.setRepairRequestStatus();

        this.repairRequestService.addRepairRequest(this.model).subscribe(
            repairRequestRes => {
                if (repairRequestRes["isSucceeded"] === true) {
                    this.model2.repairRequestId = repairRequestRes["data"].id;

                    this.visibleAnimate = false;
                    setTimeout(() => this.visible = false, 300);
                    this.isActive = false;

                    this.customDialogService.alert("Success", "Saved Successfully", false);

                    this.hide();

                } else {
                    this.isActive = false;
                    this.customDialogService.alert("Fail", "something wrong", false);
                }
            });
    }

    setRepairRequestStatus() {
        if (this.model.external === "true") {
            this.model.repairRequestStatusId = 1; // Needs approval.
        } else if (this.model.external === "false") {
            this.model.repairRequestStatusId = 5; // In progress.
        }
    }
}
