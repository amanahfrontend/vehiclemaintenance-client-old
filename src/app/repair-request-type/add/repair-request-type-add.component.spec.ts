import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestTypeAddComponent } from './repair-request-type-add.component';

describe('RepairRequestTypeAddComponent', () => {
  let component: RepairRequestTypeAddComponent;
  let fixture: ComponentFixture<RepairRequestTypeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestTypeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestTypeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
