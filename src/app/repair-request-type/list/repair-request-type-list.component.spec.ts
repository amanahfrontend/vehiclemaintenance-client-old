import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestTypeListComponent } from './repair-request-type-list.component';

describe('RepairRequestTypeListComponent', () => {
  let component: RepairRequestTypeListComponent;
  let fixture: ComponentFixture<RepairRequestTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestTypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
