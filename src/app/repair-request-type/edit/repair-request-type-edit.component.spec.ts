import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairRequestTypeEditComponent } from './repair-request-type-edit.component';

describe('RepairRequestTypeEditComponent', () => {
  let component: RepairRequestTypeEditComponent;
  let fixture: ComponentFixture<RepairRequestTypeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairRequestTypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairRequestTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
