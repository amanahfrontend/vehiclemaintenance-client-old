import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { IRepairRequest } from '../shared/Interfaces/IRepairRequest.interface';
import { RepairRequestService } from '../repair-requests/services/repair-request.service';

@Component({
    selector: 'app-external-schrepairrequest',
    templateUrl: './external_schrepairrequest.component.html',
    styleUrls: ['./external_schrepairrequest.component.css'],
    providers: [RepairRequestService]
})

export class External_SchRepairRequestComponent implements OnInit {
    repairrequests: IRepairRequest[];
    type: boolean;
    isActive: boolean = false;

    constructor(private repairrequestservice: RepairRequestService) { }

    ngOnInit() {
        this.isActive = true;
        this.type = false;
       
        this.repairrequestservice.getByExternalandMaintenanceType(true, this.type).subscribe(repairrequestRes => {
            if (repairrequestRes["isSucceeded"] === true) {
                this.repairrequests = repairrequestRes["data"];
                console.log(this.repairrequests);
                this.isActive = false;
            }
            return this.repairrequests;
        });
    }

}